#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "cmsis_armcc.h"
#include "cmsis_os2.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "portmacro.h"
#include "queue.h"
#include "semphr.h"
#include "task.h"
#include "cmsis_os2_ex.h"

#ifdef CMSIS_OS2_EX_TRACE_MALLOC_FREE
    #include <stdio.h>

    #if defined STM32F103xB
        #include "stm32f1xx_hal.h"
        #include "core_cm3.h"
    #else
        #error "Device not specified."
    #endif

#endif


#ifdef CMSIS_OS2_EX_TRACE_MALLOC_FREE

    #define CMSIS_OS2_EX_ATOMIC_RESTORESTATE for (int32_t wasMasked = __disable_irq(), flag = 1; flag; flag = __restore_irq(wasMasked))

    inline static int32_t __restore_irq(int32_t wasMasked)
    {
        if (!wasMasked)
            __enable_irq();
        return 0;
    }

    inline static void __start_timer(void)
    {
        CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk; /* Enable TRCENA bit to enable DWT and other debug and trace features. */
        DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;            /* Enable CYCCNTENA to enable CYCCNT counter. */
        DWT->CYCCNT = 0;                                /* Clear counter. */
    }

    inline static uint32_t __stop_timer(void)
    {
        uint32_t result = DWT->CYCCNT;          /* Get timer value. */
        DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk;   /* Disable timer. */

        /* TRCENA still enabled. */

        return result;
    }

#endif


/* Copy of cmsis_os2.c content. */
#if   ((__ARM_ARCH_7M__      == 1U) || \
       (__ARM_ARCH_7EM__     == 1U) || \
       (__ARM_ARCH_8M_MAIN__ == 1U))
    #define IS_IRQ_MASKED()           ((__get_PRIMASK() != 0U) || ((osKernelGetState() == osKernelRunning) && (__get_BASEPRI() != 0U)))
#elif  (__ARM_ARCH_6M__      == 1U)
    #define IS_IRQ_MASKED()           ((__get_PRIMASK() != 0U) &&  (osKernelGetState() == osKernelRunning))
#elif (__ARM_ARCH_7A__       == 1)
    #define IS_IRQ_MASKED()           (0U)
#else
    #define IS_IRQ_MASKED()           (__get_PRIMASK() != 0U)
#endif

#if    (__ARM_ARCH_7A__      == 1U)
    /* CPSR mode bitmasks */
    #define CPSR_MODE_USER            0x10U
    #define CPSR_MODE_SYSTEM          0x1FU

    #define IS_IRQ_MODE()             ((__get_mode() != CPSR_MODE_USER) && (__get_mode() != CPSR_MODE_SYSTEM))
#else
    #define IS_IRQ_MODE()             (__get_IPSR() != 0U)
#endif

#define IS_IRQ()                  (IS_IRQ_MODE() || IS_IRQ_MASKED())
/* Copy of cmsis_os2.c content. */


osStatus_t osExMessageQueuePut(osMessageQueueId_t mq_id, const void *msg_ptr, uint32_t timeout)
{
    return osMessageQueuePut(mq_id, msg_ptr, NULL, timeout);
}

osStatus_t osExMessageQueuePutTry(osMessageQueueId_t mq_id, const void *msg_ptr)
{
    return osMessageQueuePut(mq_id, msg_ptr, NULL, 0U);
}

osStatus_t osExMessageQueuePutWait(osMessageQueueId_t mq_id, const void *msg_ptr)
{
    return osMessageQueuePut(mq_id, msg_ptr, NULL, osWaitForever);
}


/* Based on cmsis_os2.c osMessageQueuePut function. */
osStatus_t osExMessageQueuePutToFront (osMessageQueueId_t mq_id, const void *msg_ptr, uint32_t timeout) {
  QueueHandle_t hQueue = (QueueHandle_t)mq_id;
  osStatus_t stat;
  BaseType_t yield;

  stat = osOK;

  if (IS_IRQ()) {
    if ((hQueue == NULL) || (msg_ptr == NULL) || (timeout != 0U)) {
      stat = osErrorParameter;
    }
    else {
      yield = pdFALSE;

      if (xQueueSendToFrontFromISR (hQueue, msg_ptr, &yield) != pdTRUE) {
        stat = osErrorResource;
      } else {
        portYIELD_FROM_ISR (yield);
      }
    }
  }
  else {
    if ((hQueue == NULL) || (msg_ptr == NULL)) {
      stat = osErrorParameter;
    }
    else {
      if (xQueueSendToFront (hQueue, msg_ptr, (TickType_t)timeout) != pdPASS) {
        if (timeout != 0U) {
          stat = osErrorTimeout;
        } else {
          stat = osErrorResource;
        }
      }
    }
  }

  return (stat);
}

osStatus_t osExMessageQueuePutToFrontTry(osMessageQueueId_t mq_id, const void *msg_ptr)
{
    return osExMessageQueuePutToFront(mq_id, msg_ptr, 0U);
}

osStatus_t osExMessageQueuePutToFrontWait(osMessageQueueId_t mq_id, const void *msg_ptr)
{
    return osExMessageQueuePutToFront(mq_id, msg_ptr, osWaitForever);
}


/* Based on cmsis_os2.c osMessageQueueGet function. */
osStatus_t osExMessageQueuePeek (osMessageQueueId_t mq_id, void *msg_ptr, uint32_t timeout) {
  QueueHandle_t hQueue = (QueueHandle_t)mq_id;
  osStatus_t stat;

  stat = osOK;

  if (IS_IRQ()) {
    if ((hQueue == NULL) || (msg_ptr == NULL) || (timeout != 0U)) {
      stat = osErrorParameter;
    }
    else {
      if (xQueuePeekFromISR (hQueue, msg_ptr) != pdPASS) {
        stat = osErrorResource;
      } else {
        portYIELD_FROM_ISR (pdFALSE);
      }
    }
  }
  else {
    if ((hQueue == NULL) || (msg_ptr == NULL)) {
      stat = osErrorParameter;
    }
    else {
      if (xQueuePeek (hQueue, msg_ptr, (TickType_t)timeout) != pdPASS) {
        if (timeout != 0U) {
          stat = osErrorTimeout;
        } else {
          stat = osErrorResource;
        }
      }
    }
  }

  return (stat);
}

osStatus_t osExMessageQueuePeekTry(osMessageQueueId_t mq_id, void *msg_ptr)
{
    return osExMessageQueuePeek(mq_id, msg_ptr, 0U);
}

osStatus_t osExMessageQueuePeekWait(osMessageQueueId_t mq_id, void *msg_ptr)
{
    return osExMessageQueuePeek(mq_id, msg_ptr, osWaitForever);
}


osStatus_t osExMessageQueueGet(osMessageQueueId_t mq_id, void *msg_ptr, uint32_t timeout)
{
    return osMessageQueueGet(mq_id, msg_ptr, NULL, timeout);
}

osStatus_t osExMessageQueueGetTry(osMessageQueueId_t mq_id, void *msg_ptr)
{
    return osMessageQueueGet(mq_id, msg_ptr, NULL, 0U);
}

osStatus_t osExMessageQueueGetWait(osMessageQueueId_t mq_id, void *msg_ptr)
{
    return osMessageQueueGet(mq_id, msg_ptr, NULL, osWaitForever);
}


osStatus_t osExMutexAcquireTry(osMutexId_t mutex_id)
{
    return osMutexAcquire(mutex_id, 0U);
}

osStatus_t osExMutexAcquireWait(osMutexId_t mutex_id)
{
    return osMutexAcquire(mutex_id, osWaitForever);
}


osStatus_t osExSemaphoreAcquireTry(osSemaphoreId_t semaphore_id)
{
    return osSemaphoreAcquire(semaphore_id, 0U);
}

osStatus_t osExSemaphoreAcquireWait(osSemaphoreId_t semaphore_id)
{
    return osSemaphoreAcquire(semaphore_id, osWaitForever);
}


#if ( configUSE_QUEUE_SETS == 1 ) /* configUSE_QUEUE_SETS should be defined in FreeRTOSConfig.h */

/* Based on cmsis_os2.c osMessageQueueNew function. */
osExMessageQueueSetId_t osExMessageQueueSetNew (uint32_t msg_count, const osExMessageQueueSetAttr_t *attr) {
  QueueHandle_t hQueue;
  #if (configQUEUE_REGISTRY_SIZE > 0)
  const char *name;
  #endif

  hQueue = NULL;

  if (!IS_IRQ() && (msg_count > 0U)) {

    hQueue = xQueueCreateSet (msg_count);

    #if (configQUEUE_REGISTRY_SIZE > 0)
    if (hQueue != NULL) {
      if (attr != NULL) {
        name = attr->name;
      } else {
        name = NULL;
      }
      vQueueAddToRegistry (hQueue, name);
    }
    #endif

  }

  return ((osExMessageQueueSetId_t)hQueue);
}

/* Based on cmsis_os2.c osMessageQueuePut function. */
osStatus_t osExMessageQueueSetAddMessageQueueOrSemaphore (osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t mqos_id) {
  QueueSetHandle_t hQueueSet = (QueueSetHandle_t)mqs_id;
  QueueSetMemberHandle_t hQueueSetMember = (QueueSetMemberHandle_t)mqos_id;
  osStatus_t stat;

  stat = osOK;

  if (IS_IRQ()) {
    stat = osErrorISR;
  }
  else {
    if ((hQueueSet == NULL) || (hQueueSetMember == NULL)) {
      stat = osErrorParameter;
    }
    else {
      if (xQueueAddToSet (hQueueSetMember, hQueueSet) != pdPASS) {
        stat = osError;
      }
    }
  }

  return (stat);
}

/* Based on cmsis_os2.c osMessageQueuePut function. */
osStatus_t osExMessageQueueSetRemoveMessageQueueOrSemaphore(osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t mqos_id) {
  QueueSetHandle_t hQueueSet = (QueueSetHandle_t)mqs_id;
  QueueSetMemberHandle_t hQueueSetMember = (QueueSetMemberHandle_t)mqos_id;
  osStatus_t stat;

  stat = osOK;

  if (IS_IRQ()) {
    stat = osErrorISR;
  }
  else {
    if ((hQueueSet == NULL) || (hQueueSetMember == NULL)) {
      stat = osErrorParameter;
    }
    else {
      if (xQueueRemoveFromSet (hQueueSetMember, hQueueSet) != pdPASS) {
        stat = osError;
      }
    }
  }

  return (stat);
}

/* Based on cmsis_os2.c osMessageQueueGet function. */
osStatus_t osExMessageQueueSetSelect (osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t *mqos_id, uint32_t timeout) {
  QueueSetHandle_t hQueueSet = (QueueSetHandle_t)mqs_id;
  QueueSetMemberHandle_t hQueueSetMember;
  osStatus_t stat;

  hQueueSetMember = NULL;
  stat = osOK;

  if (IS_IRQ()) {
    if ((hQueueSet == NULL) || (mqos_id == NULL) || (timeout != 0U)) {
      stat = osErrorParameter;
    }
    else {

      hQueueSetMember = xQueueSelectFromSetFromISR (hQueueSet);

      if (hQueueSetMember == NULL) {
        stat = osErrorResource;
      }
    }
  }
  else {
    if ((hQueueSet == NULL) || (mqos_id == NULL)) {
      stat = osErrorParameter;
    }
    else {

      hQueueSetMember = xQueueSelectFromSet (hQueueSet, (TickType_t)timeout);

      if (hQueueSetMember == NULL) {
        if (timeout != 0U) {
          stat = osErrorTimeout;
        } else {
          stat = osErrorResource;
        }
      }
    }
  }

  *mqos_id = hQueueSetMember;

  return (stat);
}

osStatus_t osExMessageQueueSetSelectTry(osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t *mqos_id)
{
    return osExMessageQueueSetSelect(mqs_id, mqos_id, 0);
}

osStatus_t osExMessageQueueSetSelectWait(osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t *mqos_id)
{
    return osExMessageQueueSetSelect(mqs_id, mqos_id, osWaitForever);
}

osStatus_t osExMessageQueueSetDelete (osExMessageQueueSetId_t mqs_id) {
  QueueSetHandle_t hQueueSet = (QueueSetHandle_t)mqs_id;
  osStatus_t stat;

#ifndef USE_FreeRTOS_HEAP_1
  if (IS_IRQ()) {
    stat = osErrorISR;
  }
  else if (hQueueSet == NULL) {
    stat = osErrorParameter;
  }
  else {
    #if (configQUEUE_REGISTRY_SIZE > 0)
    vQueueUnregisterQueue (hQueue);
    #endif

    stat = osOK;
    vQueueDelete ((QueueHandle_t)hQueueSet);
  }
#else
  stat = osError;
#endif

  return (stat);
}

#endif /* configUSE_QUEUE_SETS */


bool osExIsInISR(void)
{
    return IS_IRQ();
}

void oxExDisableInterrupts(void)
{
    taskDISABLE_INTERRUPTS();
}

void oxExEnableInterrupts(void)
{
    taskENABLE_INTERRUPTS();
}


void osExEnterCritical(void)
{
    taskENTER_CRITICAL();
}

void osExExitCritical(void)
{
    taskEXIT_CRITICAL();
}

uint32_t osExEnterCriticalInISR(void)
{
    return taskENTER_CRITICAL_FROM_ISR();
}

void osExExitCriticalInISR(uint32_t status)
{
    taskEXIT_CRITICAL_FROM_ISR(status);
}


size_t osExGetFreeHeapSize(void)
{
    return xPortGetFreeHeapSize();
}


void * osExMalloc(size_t size)
{
#ifdef CMSIS_OS2_EX_TRACE_MALLOC_FREE

    void *pResult;

    CMSIS_OS2_EX_ATOMIC_RESTORESTATE
    {
        printf("+ %s %u %u->\r\n", __FUNCTION__, size, xPortGetFreeHeapSize());

        __start_timer();

        pResult = pvPortMalloc(size);

        uint32_t ticks = __stop_timer();

        printf("+ %s ->%u %p (%u ticks)\r\n", __FUNCTION__, xPortGetFreeHeapSize(), pResult, ticks);
    }

    return pResult;

#else

    return pvPortMalloc(size);

#endif
}

void osExFree(void *ptr)
{
#ifdef CMSIS_OS2_EX_TRACE_MALLOC_FREE

    CMSIS_OS2_EX_ATOMIC_RESTORESTATE
    {
        printf("- %s %p %u->\r\n", __FUNCTION__, ptr, xPortGetFreeHeapSize());

        __start_timer();

        vPortFree(ptr);

        uint32_t ticks = __stop_timer();

        printf("- %s ->%u (%u ticks)\r\n", __FUNCTION__, xPortGetFreeHeapSize(), ticks);
    }

#else

    vPortFree(ptr);

#endif
}
