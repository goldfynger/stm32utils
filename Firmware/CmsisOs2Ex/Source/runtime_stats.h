#ifndef __RUNTIME_STATS_H
#define __RUNTIME_STATS_H


#include <stdint.h>
#include "cmsis_os2.h"


osStatus_t RUNTIME_STATS_Start(uint32_t delayTicks);

void RUNTIME_STATS_IncrementRunTimeCounterValue(void);


#endif /* __RUNTIME_STATS_H */
