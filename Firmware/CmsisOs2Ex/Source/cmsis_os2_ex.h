#ifndef __CMSIS_OS2_EX_H
#define __CMSIS_OS2_EX_H


#include <stdbool.h>
#include <stdint.h>
#include "cmsis_os2.h"
#include "FreeRTOSConfig.h"


typedef volatile enum
{
    CMSIS_OS2_EX_TASK_STATE_IDLE        = ((uint32_t)    0x00000000),
    CMSIS_OS2_EX_TASK_STATE_START       = ((uint32_t)    0x00000001),
    CMSIS_OS2_EX_TASK_STATE_RUN         = ((uint32_t)    0x00000002),
    CMSIS_OS2_EX_TASK_STATE_TERMINATED  = ((uint32_t)    0x00000003),
    CMSIS_OS2_EX_TASK_STATE_ERROR       = ((uint32_t)    0x00000004),
    CMSIS_OS2_EX_TASK_STATE_COMPLETED   = ((uint32_t)    0x00000005),

    __CMSIS_OS2_EX_TASK_STATE_UNKNOWN   = ((uint32_t)    0x7FFFFFFF),
}
CMSIS_OS2_EX_TaskStateTypeDef;


#if ( configUSE_QUEUE_SETS == 1 ) /* configUSE_QUEUE_SETS should be defined in FreeRTOSConfig.h */

/// \details Message Queue Set ID identifies the message queue set.
typedef void *osExMessageQueueSetId_t;

/// \details Message Queue Or Semaphore ID identifies the message queue or semaphore.
typedef void *osExMessageQueueOrSemaphoreId_t;


/// Attributes structure for message queue set.
typedef struct {
  const char                   *name;   ///< name of the message queue set
  uint32_t                 attr_bits;   ///< attribute bits
} osExMessageQueueSetAttr_t;

#endif /* configUSE_QUEUE_SETS */


osStatus_t  osExMessageQueuePut             (osMessageQueueId_t mq_id, const void *msg_ptr, uint32_t timeout);
osStatus_t  osExMessageQueuePutTry          (osMessageQueueId_t mq_id, const void *msg_ptr);
osStatus_t  osExMessageQueuePutWait         (osMessageQueueId_t mq_id, const void *msg_ptr);

osStatus_t  osExMessageQueuePutToFront      (osMessageQueueId_t mq_id, const void *msg_ptr, uint32_t timeout);
osStatus_t  osExMessageQueuePutToFrontTry   (osMessageQueueId_t mq_id, const void *msg_ptr);
osStatus_t  osExMessageQueuePutToFrontWait  (osMessageQueueId_t mq_id, const void *msg_ptr);

osStatus_t  osExMessageQueuePeek            (osMessageQueueId_t mq_id, void *msg_ptr, uint32_t timeout);
osStatus_t  osExMessageQueuePeekTry         (osMessageQueueId_t mq_id, void *msg_ptr);
osStatus_t  osExMessageQueuePeekWait        (osMessageQueueId_t mq_id, void *msg_ptr);

osStatus_t  osExMessageQueueGet             (osMessageQueueId_t mq_id, void *msg_ptr, uint32_t timeout);
osStatus_t  osExMessageQueueGetTry          (osMessageQueueId_t mq_id, void *msg_ptr);
osStatus_t  osExMessageQueueGetWait         (osMessageQueueId_t mq_id, void *msg_ptr);

osStatus_t  osExMutexAcquireTry             (osMutexId_t mutex_id);
osStatus_t  osExMutexAcquireWait            (osMutexId_t mutex_id);

osStatus_t  osExSemaphoreAcquireTry         (osSemaphoreId_t semaphore_id);
osStatus_t  osExSemaphoreAcquireWait        (osSemaphoreId_t semaphore_id);

#if ( configUSE_QUEUE_SETS == 1 ) /* configUSE_QUEUE_SETS should be defined in FreeRTOSConfig.h */

osExMessageQueueSetId_t osExMessageQueueSetNew                          (uint32_t msg_count, const osExMessageQueueSetAttr_t *attr);
osStatus_t              osExMessageQueueSetAddMessageQueueOrSemaphore   (osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t mqos_id);
osStatus_t              osExMessageQueueSetRemoveMessageQueueOrSemaphore(osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t mqos_id);
osStatus_t              osExMessageQueueSetSelect                       (osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t *mqos_id, uint32_t timeout);
osStatus_t              osExMessageQueueSetSelectTry                    (osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t *mqos_id);
osStatus_t              osExMessageQueueSetSelectWait                   (osExMessageQueueSetId_t mqs_id, osExMessageQueueOrSemaphoreId_t *mqos_id);
osStatus_t              osExMessageQueueSetDelete                       (osExMessageQueueSetId_t mqs_id);

#endif /* configUSE_QUEUE_SETS */

bool        osExIsInISR                     (void);
void        oxExDisableInterrupts           (void);
void        oxExEnableInterrupts            (void);

void        osExEnterCritical               (void);
void        osExExitCritical                (void);
uint32_t    osExEnterCriticalInISR          (void);
void        osExExitCriticalInISR           (uint32_t status);

size_t      osExGetFreeHeapSize             (void);

void      * osExMalloc                      (size_t size);
void        osExFree                        (void *ptr);


#endif /* __CMSIS_OS2_EX_H */
