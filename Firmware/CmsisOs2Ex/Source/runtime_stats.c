#include <stddef.h>
#include <stdint.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "FreeRTOS.h"
#include "task.h"
#include "trace.h"
#include "trace_config.h"
#include "runtime_stats.h"


#define RUNTIME_STATS_BUFFER_SIZE   256U


static osThreadId_t _runtimeStatsTaskHandle;
static osThreadAttr_t _runtimeStatsTask_attributes = {
    .name = "rtimeStatsTask",
    .attr_bits = osThreadDetached,
    .cb_mem = NULL,
    .cb_size = 0,
    .stack_mem = NULL,
    .stack_size = 128 * 4,
    .priority = (osPriority_t) osPriorityLow,
    .tz_module = 0,
    .reserved = 0,
};


uint32_t _delayTicks = 0;
uint32_t _runTimeCounterValue = 0;

char _buffer[RUNTIME_STATS_BUFFER_SIZE] = {0};


static __NO_RETURN void RUNTIME_STATS_Task(void *argument);


osStatus_t RUNTIME_STATS_Start(uint32_t delayTicks)
{
    if (_runtimeStatsTaskHandle != NULL)
    {
        return osError;
    }

    if ((_runtimeStatsTaskHandle = osThreadNew(RUNTIME_STATS_Task, NULL, &_runtimeStatsTask_attributes)) == NULL)
    {
        return osError;
    }

    _delayTicks = delayTicks;

    return osOK;
}

static __NO_RETURN void RUNTIME_STATS_Task(void *argument)
{
    static const char sTaskList[] = "TaskList:\r\n";
    static const char sTaskRunTimeStats[] = "TaskRunTimeStats:\r\n";

    const size_t taskListLength = strlen(sTaskList);
    const size_t taskRunTimeStatsLength = strlen(sTaskRunTimeStats);

    while (true)
    {
        memset(_buffer, 0, RUNTIME_STATS_BUFFER_SIZE);

        memcpy(_buffer, sTaskList, taskListLength);

        vTaskList(_buffer + taskListLength);

        if (_buffer[RUNTIME_STATS_BUFFER_SIZE - 1] != 0)
        {
            goto BufferOwerflow;
        }

        TRACE_String(_buffer);

        size_t bufferEffectiveLen = strlen(_buffer);

        TRACE_Format("----(%u)----\r\n", bufferEffectiveLen);


        memset(_buffer, 0, RUNTIME_STATS_BUFFER_SIZE);

        memcpy(_buffer, sTaskRunTimeStats, taskRunTimeStatsLength);

        vTaskGetRunTimeStats(_buffer + taskRunTimeStatsLength);

        if (_buffer[RUNTIME_STATS_BUFFER_SIZE - 1] != 0)
        {
            goto BufferOwerflow;
        }

        TRACE_String(_buffer);

        bufferEffectiveLen = strlen(_buffer);

        TRACE_Format("----(%u)----\r\n", bufferEffectiveLen);


        osDelay(_delayTicks);
    }


    BufferOwerflow:
    osExEnterCritical();
    while(true)
    {
    }
}


void RUNTIME_STATS_IncrementRunTimeCounterValue(void)
{
    _runTimeCounterValue++;
    __force_stores();
}


/* Weak function. */
void configureTimerForRunTimeStats(void)
{
    _runTimeCounterValue = 0;
    __force_stores();
}

/* Weak function. */
unsigned long getRunTimeCounterValue(void)
{
    __memory_changed();
    return _runTimeCounterValue;
}
