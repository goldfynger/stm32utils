/*

Protocol low level:
-------------------------------------------------------------------------------------------
| DLE (1) | STX (1) | Protocol high level data. DLE bytes duplicates  | DLE (1) | ETX (1) |
-------------------------------------------------------------------------------------------

Protocol high level:
-------------------------------------------------------------------------------------------
|             Application level data              |  CRC32 of application level data (4)  |
-------------------------------------------------------------------------------------------

STREAM_TO_PACKET_MakePacket converts stream to packet and can be called several times with same handle to collect complete packet using serial parts of stream.
STREAM_TO_PACKET_MakeStream converts complete packet to complete stream.

Buffer expanding algorithm:
64              (128)   0x0000_0000_0100_0000   (STREAM_TO_PACKET_BUFFER_MIN_STEP)
64 + 64         (128)   0x0000_0000_1000_0000
128 + 64        (192)   0x0000_0000_1100_0000
192 + 64        (256)   0x0000_0001_0000_0000
256 + 128       (384)   0x0000_0001_1000_0000
384 + 128       (512)   0x0000_0010_0000_0000
....
16384 + 8192    (24576) 0x0110_0000_0000_0000
24576 + 8192    (32768) 0x1000_0000_0000_0000
32768 + 16384   (49152) 0x1100_0000_0000_0000   (STREAM_TO_PACKET_BUFFER_MAX_SIZE)

*/


#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "crc.h"
#include "stream_to_packet.h"

#if defined STM32F103xB
    #include "stm32f1xx_hal.h"
#else
    #error "Device not specified."
#endif


#define STREAM_TO_PACKET_SYMBOL_STX                 0x02U
#define STREAM_TO_PACKET_SYMBOL_ETX                 0x03U
#define STREAM_TO_PACKET_SYMBOL_DLE                 0x10U

#define STREAM_TO_PACKET_BUFFER_MIN_STEP            64U                 /* 0x0000_0000_0100_0000 */
#define STREAM_TO_PACKET_BUFFER_MAX_SIZE            (32768U + 16384U)   /* 0x1100_0000_0000_0000 */

#define STREAM_TO_PACKET_REQUIRED_DLE_STX_ETX_SIZE  4U
#define STREAM_TO_PACKET_CRC32_SIZE                 4U


static STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_AddToBuffer       (STREAM_TO_PACKET_HandleTypeDef *hStreamToPacket, uint_fast8_t value);
static STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_ClearBuffer       (STREAM_TO_PACKET_HandleTypeDef *hStreamToPacket);
static STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_CheckCrc32        (STREAM_TO_PACKET_HandleTypeDef *hStreamToPacket, bool *pResult);


/* pStream and ppPacket must be freed by calling code.
 * hStreamToPacket handle must be zeroed before first time call.
 * ppPacket is NULL if packet is not ready yet. In that case return value is STREAM_TO_PACKET_ERR_OK so ppPacket must be checked for NULL in calling code. */
STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_MakePacket(STREAM_TO_PACKET_HandleTypeDef *hStreamToPacket, STREAM_TO_PACKET_StreamTypeDef *pStream, STREAM_TO_PACKET_PacketTypeDef **ppPacket)
{
    if (hStreamToPacket == NULL)
    {
        return STREAM_TO_PACKET_ERR_INVALID_PARAMETER;
    }

    if (pStream == NULL || pStream->Size == 0 || pStream->Offset >= pStream->Size)
    {
        return STREAM_TO_PACKET_ERR_INVALID_PARAMETER;
    }

    if (ppPacket == NULL || *ppPacket != NULL)
    {
        return STREAM_TO_PACKET_ERR_INVALID_PARAMETER;
    }


    STREAM_TO_PACKET_ErrorTypeDef error = STREAM_TO_PACKET_ERR_OK;

    while (pStream->Offset < pStream->Size)
    {
        uint8_t value = pStream->Data[pStream->Offset];
        pStream->Offset += 1;

        switch (hStreamToPacket->State)
        {
            case (STREAM_TO_PACKET_STATE_ERROR):
            {
                if ((error = STREAM_TO_PACKET_ClearBuffer(hStreamToPacket)) != STREAM_TO_PACKET_ERR_OK)
                {
                    return error;
                }

                /* Switch to DLE waiting state. */
                hStreamToPacket->State = STREAM_TO_PACKET_STATE_WAIT_FOR_FIRST_DLE;
            }
            /* No break. */


            case (STREAM_TO_PACKET_STATE_WAIT_FOR_FIRST_DLE):
            {
                if (value == STREAM_TO_PACKET_SYMBOL_DLE)
                {
                    /* Switch to STX waiting state. */
                    hStreamToPacket->State = STREAM_TO_PACKET_STATE_WAIT_FOR_STX;
                }

                /* Ignore other symbols. */
            }
            break;


            case (STREAM_TO_PACKET_STATE_WAIT_FOR_STX):
            {
                if (value == STREAM_TO_PACKET_SYMBOL_STX)
                {
                    /* Switch to DATA waiting state. */
                    hStreamToPacket->State = STREAM_TO_PACKET_STATE_WAIT_FOR_DATA;
                }
                else
                {
                    /* Switch to DLE waiting state. */
                    hStreamToPacket->State = STREAM_TO_PACKET_STATE_WAIT_FOR_FIRST_DLE;
                }
            }
            break;


            case (STREAM_TO_PACKET_STATE_WAIT_FOR_DATA):
            {
                if (value == STREAM_TO_PACKET_SYMBOL_DLE)
                {
                    /* Switch to ETX or DLE waiting state. */
                    hStreamToPacket->State = STREAM_TO_PACKET_STATE_WAIT_FOR_ETX_OR_SECOND_DLE;
                }
                else
                {
                    /* Add byte to buffer. */
                    if ((error = STREAM_TO_PACKET_AddToBuffer(hStreamToPacket, value)) != STREAM_TO_PACKET_ERR_OK)
                    {
                        hStreamToPacket->State = STREAM_TO_PACKET_STATE_ERROR;

                        return error;
                    }
                }
            }
            break;


            case (STREAM_TO_PACKET_STATE_WAIT_FOR_ETX_OR_SECOND_DLE):
            {
                if (value == STREAM_TO_PACKET_SYMBOL_ETX)
                {
                    if ((((int32_t)hStreamToPacket->BufferIndex) - STREAM_TO_PACKET_CRC32_SIZE) > 0)
                    {
                        /* Check CRC32. */
                        bool checkResult = false;
                        if ((error = STREAM_TO_PACKET_CheckCrc32(hStreamToPacket, &checkResult)) != STREAM_TO_PACKET_ERR_OK)
                        {
                            hStreamToPacket->State = STREAM_TO_PACKET_STATE_ERROR;

                            return error;
                        }

                        /* Allocate packet and copy data from buffer. */
                        if (checkResult)
                        {
                            size_t dataSize = hStreamToPacket->BufferIndex - STREAM_TO_PACKET_CRC32_SIZE;

                            STREAM_TO_PACKET_PacketTypeDef *pPacket = (STREAM_TO_PACKET_PacketTypeDef *)osExMalloc(STREAM_TO_PACKET_GetPacketSize(dataSize));

                            if (pPacket == NULL)
                            {
                                hStreamToPacket->State = STREAM_TO_PACKET_STATE_ERROR;

                                return STREAM_TO_PACKET_ERR_MEMORY_CANNOT_BE_ALLOCATED;
                            }

                            memset(pPacket, 0, sizeof(STREAM_TO_PACKET_PacketTypeDef)); /* Clear packet except Data field. */

                            pPacket->Size = dataSize;
                            memcpy(pPacket->Data, hStreamToPacket->Buffer, dataSize);

                            *ppPacket = pPacket;
                        }
                    }

                    /* Switch to DLE waiting state. */
                    hStreamToPacket->State = STREAM_TO_PACKET_STATE_WAIT_FOR_FIRST_DLE;

                    if ((error = STREAM_TO_PACKET_ClearBuffer(hStreamToPacket)) != STREAM_TO_PACKET_ERR_OK)
                    {
                        hStreamToPacket->State = STREAM_TO_PACKET_STATE_ERROR;

                        return error;
                    }

                    /* Packet complete. */
                    if (*ppPacket != NULL)
                    {
                        return STREAM_TO_PACKET_ERR_OK;
                    }

                    /* Else continue. */
                }
                else if (value == STREAM_TO_PACKET_SYMBOL_DLE)
                {
                    /* Add DLE to buffer. */
                    if ((error = STREAM_TO_PACKET_AddToBuffer(hStreamToPacket, value)) != STREAM_TO_PACKET_ERR_OK)
                    {
                        hStreamToPacket->State = STREAM_TO_PACKET_STATE_ERROR;

                        return error;
                    }

                    /* Switch to DATA waiting state. */
                    hStreamToPacket->State = STREAM_TO_PACKET_STATE_WAIT_FOR_DATA;
                }
                else if (value == STREAM_TO_PACKET_SYMBOL_STX)
                {
                    /* Switch to DATA waiting state. */
                    hStreamToPacket->State = STREAM_TO_PACKET_STATE_WAIT_FOR_DATA;

                    if ((error = STREAM_TO_PACKET_ClearBuffer(hStreamToPacket)) != STREAM_TO_PACKET_ERR_OK)
                    {
                        hStreamToPacket->State = STREAM_TO_PACKET_STATE_ERROR;

                        return error;
                    }
                }
                else
                {
                    /* Switch to DLE waiting state. */
                    hStreamToPacket->State = STREAM_TO_PACKET_STATE_WAIT_FOR_FIRST_DLE;

                    if ((error = STREAM_TO_PACKET_ClearBuffer(hStreamToPacket)) != STREAM_TO_PACKET_ERR_OK)
                    {
                        hStreamToPacket->State = STREAM_TO_PACKET_STATE_ERROR;

                        return error;
                    }
                }
            }
            break;
        }
    }

    return STREAM_TO_PACKET_ERR_OK;
}

/* pPacket and ppStream must be freed by calling code.
 * If return value is STREAM_TO_PACKET_ERR_OK ppStream is not NULL. */
STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_MakeStream(STREAM_TO_PACKET_PacketTypeDef *pPacket, STREAM_TO_PACKET_StreamTypeDef **ppStream)
{
    if (pPacket == NULL || pPacket->Size == 0)
    {
        return STREAM_TO_PACKET_ERR_INVALID_PARAMETER;
    }

    if (ppStream == NULL || *ppStream != NULL)
    {
        return STREAM_TO_PACKET_ERR_INVALID_PARAMETER;
    }


    uint32_t computedCrc32 = 0;
    STREAM_TO_PACKET_ErrorTypeDef error = STREAM_TO_PACKET_CalculateCrc32(pPacket->Data, pPacket->Size, &computedCrc32);

    if (error != STREAM_TO_PACKET_ERR_OK)
    {
        return error;
    }


    size_t dataSize = pPacket->Size + STREAM_TO_PACKET_CRC32_SIZE + STREAM_TO_PACKET_REQUIRED_DLE_STX_ETX_SIZE;

    for (uint16_t idx = 0; idx < pPacket->Size; idx++) /* Count number of DLE's in packet. */
    {
        if (pPacket->Data[idx] == STREAM_TO_PACKET_SYMBOL_DLE)
        {
            dataSize += 1;
        }
    }

    for (uint8_t idx = 0; idx < STREAM_TO_PACKET_CRC32_SIZE; idx++) /* Count number of DLE's in CRC32. */
    {
        if (((uint8_t *)(&computedCrc32))[idx] == STREAM_TO_PACKET_SYMBOL_DLE)
        {
            dataSize += 1;
        }
    }

    size_t streamSize = STREAM_TO_PACKET_GetStreamSize(dataSize);

    if (streamSize > UINT16_MAX)
    {
        return STREAM_TO_PACKET_ERR_STREAM_SIZE_TOO_LARGE;
    }

    STREAM_TO_PACKET_StreamTypeDef *pStream = (STREAM_TO_PACKET_StreamTypeDef *)osExMalloc(streamSize);

    if (pStream == NULL)
    {
        return STREAM_TO_PACKET_ERR_MEMORY_CANNOT_BE_ALLOCATED;
    }


    uint16_t dstIdx = 0;

    pStream->Data[dstIdx++] = STREAM_TO_PACKET_SYMBOL_DLE;
    pStream->Data[dstIdx++] = STREAM_TO_PACKET_SYMBOL_STX;

    for (uint16_t srcIdx = 0; srcIdx < pPacket->Size; srcIdx++) /* Copy data from packet and insert additional DLE's. */
    {
        uint8_t src = pPacket->Data[srcIdx];
        pStream->Data[dstIdx++] = src;

        if (src == STREAM_TO_PACKET_SYMBOL_DLE)
        {
            pStream->Data[dstIdx++] = STREAM_TO_PACKET_SYMBOL_DLE;
        }
    }

    for (uint8_t srcIdx = 0; srcIdx < STREAM_TO_PACKET_CRC32_SIZE; srcIdx++) /* Copy CRC32 and insert additional DLE's. */
    {
        uint8_t src = ((uint8_t *)(&computedCrc32))[srcIdx];
        pStream->Data[dstIdx++] = src;

        if (src == STREAM_TO_PACKET_SYMBOL_DLE)
        {
            pStream->Data[dstIdx++] = STREAM_TO_PACKET_SYMBOL_DLE;
        }
    }

    pStream->Data[dstIdx++] = STREAM_TO_PACKET_SYMBOL_DLE;
    pStream->Data[dstIdx++] = STREAM_TO_PACKET_SYMBOL_ETX;

    pStream->Offset = 0;
    pStream->Size = dstIdx;

    *ppStream = pStream;


    return STREAM_TO_PACKET_ERR_OK;
}

static STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_AddToBuffer(STREAM_TO_PACKET_HandleTypeDef *hStreamToPacket, uint_fast8_t value)
{
    /* Allocate new buffer. */
    if (hStreamToPacket->Buffer == NULL)
    {
        size_t bufferSize = STREAM_TO_PACKET_BUFFER_MIN_STEP;

        uint8_t *pBuffer = (uint8_t *)osExMalloc(bufferSize);

        if (pBuffer == NULL)
        {
            return STREAM_TO_PACKET_ERR_MEMORY_CANNOT_BE_ALLOCATED;
        }

        memset(pBuffer, 0, bufferSize);

        hStreamToPacket->Buffer = pBuffer;
        hStreamToPacket->BufferIndex = 0;
        hStreamToPacket->BufferSize = bufferSize;
    }
    /* Enlarge existing buffer. */
    else if (hStreamToPacket->BufferIndex == hStreamToPacket->BufferSize)
    {
        size_t bufferSize = hStreamToPacket->BufferSize;
        size_t enlargedBufferSize = SIZE_MAX;

        if (bufferSize <= STREAM_TO_PACKET_BUFFER_MIN_STEP)
        {
            enlargedBufferSize = STREAM_TO_PACKET_BUFFER_MIN_STEP * 2;
        }
        else if (bufferSize >= STREAM_TO_PACKET_BUFFER_MAX_SIZE)
        {
            return STREAM_TO_PACKET_ERR_BUFFER_CANNOT_BE_ENLARGED;
        }
        else
        {
            for (uint8_t shift = 15; shift > 0; shift--)    /* Example: bufferSize is 128 (0x0000_1000_0000) or 192 (0x0000_1100_0000) and shift is 7 for both values. */
            {
                if (bufferSize & (1 << shift))
                {
                    if (bufferSize & (1 << (shift - 1)))                            /* Bit (7 - 1) is set - we have bufferSize 192 (0x0000_1100_0000). */
                    {
                        enlargedBufferSize = (1 << (shift + 1));                    /* enlargedBufferSize now with shift (7 + 1) - 256 (0x0001_0000_0000). */
                    }
                    else                                                            /* Bit (7 - 1) is not set - we have bufferSize 128 (0x0000_1000_0000). */
                    {
                        enlargedBufferSize = ((1 << shift) | (1 << (shift - 1)));   /* enlargedBufferSize now with shift ((7) | (7 - 1)) - 192 (0x0000_1100_0000). */
                    }
                }
            }
        }                                                   /* Max value of enlargedBufferSize is shift ((15) | (15 - 1)) - 49152 (0x1100_0000_0000_0000) - this is STREAM_TO_PACKET_BUFFER_MAX_SIZE */

        uint8_t *pBuffer = (uint8_t *)osExMalloc(enlargedBufferSize);

        if (pBuffer == NULL)
        {
            return STREAM_TO_PACKET_ERR_MEMORY_CANNOT_BE_ALLOCATED;
        }

        memset(pBuffer, 0, enlargedBufferSize);

        memcpy(pBuffer, hStreamToPacket->Buffer, hStreamToPacket->BufferSize);

        osExFree(hStreamToPacket->Buffer);

        hStreamToPacket->Buffer = pBuffer;
        hStreamToPacket->BufferSize = enlargedBufferSize;
    }

    hStreamToPacket->Buffer[hStreamToPacket->BufferIndex] = (uint8_t)value;
    hStreamToPacket->BufferIndex += 1;

    return STREAM_TO_PACKET_ERR_OK;
}

static STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_ClearBuffer(STREAM_TO_PACKET_HandleTypeDef *hStreamToPacket)
{
    if (hStreamToPacket->Buffer != NULL)
    {
        if (hStreamToPacket->BufferSize != STREAM_TO_PACKET_BUFFER_MIN_STEP)
        {
            osExFree(hStreamToPacket->Buffer);
            hStreamToPacket->Buffer = NULL;
            hStreamToPacket->BufferIndex = 0;
            hStreamToPacket->BufferSize = 0;
        }
        else
        {
            memset(hStreamToPacket->Buffer, 0, STREAM_TO_PACKET_BUFFER_MIN_STEP);
            hStreamToPacket->BufferIndex = 0;
        }
    }
    /* Else BufferIndex and BufferSize should be 0 too. */

    return STREAM_TO_PACKET_ERR_OK;
}

static STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_CheckCrc32(STREAM_TO_PACKET_HandleTypeDef *hStreamToPacket, bool *pResult)
{
    uint8_t crc32_b3 = hStreamToPacket->Buffer[hStreamToPacket->BufferIndex - 1];
    uint8_t crc32_b2 = hStreamToPacket->Buffer[hStreamToPacket->BufferIndex - 2];
    uint8_t crc32_b1 = hStreamToPacket->Buffer[hStreamToPacket->BufferIndex - 3];
    uint8_t crc32_b0 = hStreamToPacket->Buffer[hStreamToPacket->BufferIndex - 4];

    uint32_t originalCrc32 = crc32_b0 | (crc32_b1 << 8) | (crc32_b2 << 16) | (crc32_b3 << 24);

    uint32_t computedCrc32 = 0;
    STREAM_TO_PACKET_ErrorTypeDef error = STREAM_TO_PACKET_CalculateCrc32(hStreamToPacket->Buffer, hStreamToPacket->BufferIndex - 4, &computedCrc32);

    if (error != STREAM_TO_PACKET_ERR_OK)
    {
        return error;
    }

    if (originalCrc32 == computedCrc32)
    {
        *pResult = true;
    }

    return STREAM_TO_PACKET_ERR_OK;
}


__weak STREAM_TO_PACKET_ErrorTypeDef STREAM_TO_PACKET_CalculateCrc32(uint8_t buffer[], uint_fast16_t bufferSize, uint32_t *pCrc32)
{
    (void)buffer;
    (void)bufferSize;
    (void)pCrc32;

    return STREAM_TO_PACKET_ERR_NOT_IMPLEMENTED;
}
