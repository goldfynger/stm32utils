#ifndef __STREAM_TO_PACKET_H
#define __STREAM_TO_PACKET_H


#include <stddef.h>
#include <stdint.h>


typedef enum
{
    STREAM_TO_PACKET_ERR_OK                             = ((uint8_t)0),
    STREAM_TO_PACKET_ERR_INVALID_PARAMETER,
    STREAM_TO_PACKET_ERR_MEMORY_CANNOT_BE_ALLOCATED,
    STREAM_TO_PACKET_ERR_BUFFER_CANNOT_BE_ENLARGED,
    STREAM_TO_PACKET_ERR_HAL_CRC32,
    STREAM_TO_PACKET_ERR_STREAM_SIZE_TOO_LARGE,
    STREAM_TO_PACKET_ERR_NOT_IMPLEMENTED,
}
STREAM_TO_PACKET_ErrorTypeDef;

typedef enum
{
    STREAM_TO_PACKET_STATE_WAIT_FOR_FIRST_DLE           = ((uint8_t)0),
    STREAM_TO_PACKET_STATE_WAIT_FOR_STX,
    STREAM_TO_PACKET_STATE_WAIT_FOR_DATA,
    STREAM_TO_PACKET_STATE_WAIT_FOR_ETX_OR_SECOND_DLE,
    STREAM_TO_PACKET_STATE_ERROR,
}
STREAM_TO_PACKET_StateBaseTypeDef;


typedef struct
{
    uint8_t                             *Buffer;

    uint16_t                            BufferIndex;
    uint16_t                            BufferSize;

    STREAM_TO_PACKET_StateBaseTypeDef   State;
}
STREAM_TO_PACKET_HandleTypeDef; /* Used to convert stream to packet. */

typedef struct
{
    uint16_t    Offset;     /* Stream start offset. This field incremented by stream reader. */
    uint16_t    Size;       /* Size of data that contained in Data array (full size of stream data). This field incremented by stream writer. */

    uint8_t     Data[];     /* Stream data. */
}
STREAM_TO_PACKET_StreamTypeDef; /* Contains data in stream representation. */

typedef struct
{
    uint16_t    __Reserved;
    uint16_t    Size;       /* Size of data that contained in Data array (full size of packet data). */

    uint8_t     Data[];     /* Packet data. */
}
STREAM_TO_PACKET_PacketTypeDef; /* Contains data in packet representation. */


        STREAM_TO_PACKET_ErrorTypeDef   STREAM_TO_PACKET_MakePacket     (STREAM_TO_PACKET_HandleTypeDef *hStreamToPacket, STREAM_TO_PACKET_StreamTypeDef *pStream, STREAM_TO_PACKET_PacketTypeDef **ppPacket);
        STREAM_TO_PACKET_ErrorTypeDef   STREAM_TO_PACKET_MakeStream     (STREAM_TO_PACKET_PacketTypeDef *pPacket, STREAM_TO_PACKET_StreamTypeDef **ppStream);

__weak  STREAM_TO_PACKET_ErrorTypeDef   STREAM_TO_PACKET_CalculateCrc32 (uint8_t buffer[], uint_fast16_t bufferSize, uint32_t *pCrc32);

/* Inline functions. */
inline size_t STREAM_TO_PACKET_GetStreamSize(uint_fast16_t dataSize) { return sizeof(STREAM_TO_PACKET_StreamTypeDef) + dataSize; }
inline size_t STREAM_TO_PACKET_GetPacketSize(uint_fast16_t dataSize) { return sizeof(STREAM_TO_PACKET_PacketTypeDef) + dataSize; }


#endif /* __STREAM_TO_PACKET_H */
