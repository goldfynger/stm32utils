#ifndef __TRACE_H
#define __TRACE_H


#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include "trace_config.h"

#ifdef TRACE_CONFIG_OS2
    #include "cmsis_os2.h"
    #include "cmsis_os2_ex.h"
#endif

#if defined STM32F030x6
    #include "stm32f0xx_hal.h"
#elif defined STM32F103xB
    #include "stm32f1xx_hal.h"
#else
    #error "Device not specified."
#endif


#ifdef TRACE_CONFIG_OS2
    osStatus_t                      TRACE_InitialiseOs(osPriority_t maxTaskPriority);
    osStatus_t                      TRACE_DeinitialiseOs(void);
    CMSIS_OS2_EX_TaskStateTypeDef   TRACE_GetTaskState(void);
#endif

bool TRACE_String(const char *str);
bool TRACE_Format(const char *fmt, ...);
bool TRACE_VFormat(const char *fmt, va_list args);


#endif /* __TRACE_H */
