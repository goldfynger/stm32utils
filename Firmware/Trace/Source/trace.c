#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include "trace_config.h"
#include "trace.h"

#ifdef TRACE_CONFIG_OS2
    #include "cmsis_os2.h"
    #include "cmsis_os2_ex.h"
#endif

#if defined STM32F030x6
    #include "stm32f0xx_hal.h"
#elif defined STM32F103xB
    #include "stm32f1xx_hal.h"
#else
    #error "Device not specified."
#endif


/* See AN4989 for information about trace via SWO and UART and IDE settings. */
/* https://www.st.com/resource/en/application_note/dm00354244-stm32-microcontroller-debug-toolbox-stmicroelectronics.pdf */

#ifdef TRACE_CONFIG_SERIAL
extern UART_HandleTypeDef TRACE_CONFIG_UART_HANDLE;
#endif


#ifndef TRACE_CONFIG_OS2
#if defined TRACE_CONFIG_ATOMIC || defined TRACE_CONFIG_ADD_STRING_NUMBER
    inline static int32_t __restore_irq(int32_t wasMasked)
    {
        if (!wasMasked)
        {
            __enable_irq();
        }
        return 0;
    }

    #define TRACE_ATOMIC_RESTORESTATE for (int32_t wasMasked = __disable_irq(), flag = 1; flag; flag = __restore_irq(wasMasked))
#endif
#endif

#ifdef TRACE_CONFIG_ADD_STRING_NUMBER
    static uint32_t _stringNumber = 0;

    uint32_t TRACE_GetStringNumber(void)
    {
        #ifdef TRACE_CONFIG_OS2
            return ++_stringNumber;
        #else
            uint32_t temp;

            TRACE_ATOMIC_RESTORESTATE
            {
                temp = ++_stringNumber;
            }

            return temp;
        #endif
    }
#endif

#if defined TRACE_CONFIG_SWO || defined TRACE_CONFIG_SERIAL
    int fputc(int ch, FILE *f)
    {

    #ifdef TRACE_CONFIG_SWO

        ITM_SendChar(ch);

    #endif

    #ifdef TRACE_CONFIG_SERIAL

        if (HAL_UART_Transmit(&TRACE_CONFIG_UART_HANDLE, (uint8_t *)&ch, sizeof(uint8_t), HAL_MAX_DELAY) != HAL_OK)
        {
            return EOF; /* In case of fault returns EOF even SWO trace was successful. */
        }

    #endif

        return(ch);
    }
#endif


    static int TRACE_printf(const char *fmt, ...)
    {
    #ifdef TRACE_CONFIG_ADD_STRING_NUMBER

        int preResult = printf("%u: ", TRACE_GetStringNumber());

        if (preResult < 0) return preResult;

        va_list args;
        va_start(args, fmt);

        int result = vprintf(fmt, args);

        va_end(args);

        if (result < 0) return result;

        return preResult + result;

    #else

        va_list args;
        va_start(args, fmt);

        int result = vprintf(fmt, args);

        va_end(args);

        return result;

    #endif
    }

#ifndef TRACE_CONFIG_OS2
    static int TRACE_vprintf(const char *fmt, __va_list args)
    {
    #ifdef TRACE_CONFIG_ADD_STRING_NUMBER

        int preResult = printf("%u: ", TRACE_GetStringNumber());

        if (preResult < 0) return preResult;

        int result = vprintf(fmt, args);

        if (result < 0) return result;

        return preResult + result;

    #else

        return vprintf(fmt, args);

    #endif
    }
#endif


#ifdef TRACE_CONFIG_OS2

#ifndef TRACE_CONFIG_FORMAT_DEFAULT_SIZE
    #define TRACE_CONFIG_FORMAT_DEFAULT_SIZE    64U
#endif

#ifndef TRACE_CONFIG_FORMAT_MAX_SIZE
    #define TRACE_CONFIG_FORMAT_MAX_SIZE        128U
#endif

#ifndef TRACE_CONFIG_QUEUE_COUNT
    #define TRACE_CONFIG_QUEUE_COUNT            16U
#endif


#if TRACE_CONFIG_FORMAT_DEFAULT_SIZE <= 0
    #error "TRACE_CONFIG_FORMAT_DEFAULT_SIZE must be greater than 0."
#endif

#if TRACE_CONFIG_FORMAT_MAX_SIZE < TRACE_CONFIG_FORMAT_DEFAULT_SIZE
    #error "TRACE_CONFIG_FORMAT_MAX_SIZE must be equal or greater than TRACE_CONFIG_FORMAT_DEFAULT_SIZE."
#endif

#if TRACE_CONFIG_QUEUE_COUNT <= 0
    #error "TRACE_CONFIG_QUEUE_COUNT must be greater than 0"
#endif


typedef struct
{
    const char *String;
    bool AllocatedOnHeap;
}
TRACE_QueueMessageTypeDef;


static osThreadId_t _traceTaskHandle;
static osThreadAttr_t _traceTask_attributes = {
    .name = "traceTask",
    .attr_bits = osThreadDetached,
    .cb_mem = NULL,
    .cb_size = 0,
    .stack_mem = NULL,
    .stack_size = 128 * 4,
    .priority = (osPriority_t) osPriorityNone,
    .tz_module = 0,
    .reserved = 0,
};

static osMessageQueueId_t _traceQueueHandle;
static const osMessageQueueAttr_t _traceQueue_attributes = {
    .name = NULL,
    .attr_bits = 0,
    .cb_mem = NULL,
    .cb_size = 0,
    .mq_mem = NULL,
    .mq_size = 0,
};

static CMSIS_OS2_EX_TaskStateTypeDef _traceTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;


static __NO_RETURN void TRACE_Task(void *argument);
static void TRACE_OsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus);
static void TRACE_Error(const char *pFunctionName, uint32_t line);


osStatus_t TRACE_InitialiseOs(osPriority_t maxTaskPriority)
{
    osStatus_t osStatus = osOK;


    if ((osStatus = TRACE_DeinitialiseOs()) != osOK)
    {
        return osStatus;
    }


    if ((_traceQueueHandle = osMessageQueueNew(TRACE_CONFIG_QUEUE_COUNT, sizeof(TRACE_QueueMessageTypeDef), &_traceQueue_attributes)) == NULL)
    {
        osStatus = osError;
        TRACE_OsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }


    _traceTask_attributes.priority = maxTaskPriority;

    if ((_traceTaskHandle = osThreadNew(TRACE_Task, NULL, &_traceTask_attributes)) == NULL)
    {
        osStatus = osError;
        TRACE_OsError(__FUNCTION__, __LINE__, osStatus);
        goto DeinitialiseOsAndReturnStatus;
    }


    return osOK;


    DeinitialiseOsAndReturnStatus:
    TRACE_DeinitialiseOs();
    return osStatus;
}

osStatus_t TRACE_DeinitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_traceTaskHandle != NULL)
    {
        if ((osStatus = osThreadTerminate(_traceTaskHandle)) != osOK)
        {
            TRACE_OsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _traceTaskHandle = NULL;
    }


    if (_traceQueueHandle != NULL)
    {
        if ((osStatus = osMessageQueueDelete(_traceQueueHandle)) != osOK)
        {
            TRACE_OsError(__FUNCTION__, __LINE__, osStatus);
            return osStatus;
        }

        _traceQueueHandle = NULL;
    }


    _traceTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
    __force_stores();


    return osOK;
}

/* Possible states are:
 * CMSIS_OS2_EX_TASK_STATE_IDLE - trace task is not initialised yet or already deinitialised.
 * CMSIS_OS2_EX_TASK_STATE_RUN - trace task in running state.
 * CMSIS_OS2_EX_TASK_STATE_ERROR - trace task closed in case of error.
*/
CMSIS_OS2_EX_TaskStateTypeDef TRACE_GetTaskState(void)
{
    __memory_changed();
    return _traceTaskState;
}


static __NO_RETURN void TRACE_Task(void *argument)
{
    /* Define variable here to prevent goto warning. */
    osStatus_t osStatus = osOK;


    if (TRACE_printf("%s started.\r\n", __FUNCTION__) < 0)
    {
        TRACE_Error(__FUNCTION__, __LINE__);
        goto TaskError;
    }

    _traceTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;
    __force_stores();


    TRACE_QueueMessageTypeDef message;

    while (true)
    {
        if ((osStatus = osExMessageQueueGetWait(_traceQueueHandle, &message)) != osOK)
        {
            TRACE_OsError(__FUNCTION__, __LINE__, osStatus);
            goto TaskError;
        }


        if (TRACE_printf("%s", message.String) < 0)
        {
            TRACE_Error(__FUNCTION__, __LINE__);
            goto TaskError;
        }


        if (message.AllocatedOnHeap)
        {
            osExFree((void *)message.String);
        }
    }


    TaskError:
    _traceTaskState = CMSIS_OS2_EX_TASK_STATE_ERROR;
    __force_stores();

    osThreadExit();
}

bool TRACE_String(const char *str)
{
    if (_traceTaskState != CMSIS_OS2_EX_TASK_STATE_RUN)
    {
        return false;
    }


    osStatus_t osStatus = osOK;


    TRACE_QueueMessageTypeDef message =
    {
        .String = str,
        .AllocatedOnHeap = false
    };

    if ((osStatus = osExMessageQueuePutTry(_traceQueueHandle, &message)) != osOK)
    {
        TRACE_OsError(__FUNCTION__, __LINE__, osStatus);
        return false;
    }

    return true;
}

bool TRACE_Format(const char *fmt, ...)
{
    if (_traceTaskState != CMSIS_OS2_EX_TASK_STATE_RUN)
    {
        return false;
    }


    va_list args;
    va_start(args, fmt);

    bool result = TRACE_VFormat(fmt, args);

    va_end(args);

    return result;
}

bool TRACE_VFormat(const char *fmt, va_list args)
{
    if (_traceTaskState != CMSIS_OS2_EX_TASK_STATE_RUN)
    {
        return false;
    }


    size_t bufferSize = TRACE_CONFIG_FORMAT_DEFAULT_SIZE;

    char *pBuffer = (char *)osExMalloc(bufferSize);

    if (pBuffer == NULL)
    {
        TRACE_Error(__FUNCTION__, __LINE__);
        return false;
    }


    /* Result is full theoretical length of result string w/o ending \0 (even if buffer length is not enough to store full string). */
    int length = vsnprintf(pBuffer, bufferSize, fmt, args);

    if (length < 0)
    {
        TRACE_Error(__FUNCTION__, __LINE__);
        return false;
    }


    /* Here we can realloc buffer using full length and call snprintf again. */
    if (length >= bufferSize && TRACE_CONFIG_FORMAT_DEFAULT_SIZE < TRACE_CONFIG_FORMAT_MAX_SIZE)
    {
        osExFree(pBuffer);

        bufferSize = length + 1; /* String length + \0.*/

        if (bufferSize > TRACE_CONFIG_FORMAT_MAX_SIZE)
        {
            bufferSize = TRACE_CONFIG_FORMAT_MAX_SIZE;
        }

        pBuffer = (char *)osExMalloc(bufferSize);


        int length = vsnprintf(pBuffer, bufferSize, fmt, args);

        if (length < 0)
        {
            TRACE_Error(__FUNCTION__, __LINE__);
            return false;
        }
    }


    osStatus_t osStatus = osOK;


    TRACE_QueueMessageTypeDef message =
    {
        .String = pBuffer,
        .AllocatedOnHeap = true
    };

    if ((osStatus = osExMessageQueuePutTry(_traceQueueHandle, &message)) != osOK)
    {
        TRACE_OsError(__FUNCTION__, __LINE__, osStatus);
        return false;
    }


    return true;
}

static void TRACE_OsError(const char *pFunctionName, uint32_t line, osStatus_t osStatus)
{
    TRACE_printf("OS error in %s at %u: %d.\r\n", pFunctionName, line, osStatus);
}

static void TRACE_Error(const char *pFunctionName, uint32_t line)
{
    TRACE_printf("Error in %s at %u.\r\n", pFunctionName, line);
}

#else

bool TRACE_String(const char *str)
{
#ifdef TRACE_CONFIG_ATOMIC

    bool result = false;

    TRACE_ATOMIC_RESTORESTATE
    {
        result = TRACE_printf("%s", str) >= 0;
    }

    return result;

#else

    return TRACE_printf("%s", str) >= 0;

#endif
}

bool TRACE_Format(const char *fmt, ...)
{
#ifdef TRACE_CONFIG_ATOMIC

    bool result = false;

    TRACE_ATOMIC_RESTORESTATE
    {
        va_list args;
        va_start(args, fmt);

        result = TRACE_vprintf(fmt, args) >= 0;

        va_end(args);
    }

    return result;

#else

    va_list args;
    va_start(args, fmt);

    bool result = TRACE_vprintf(fmt, args) >= 0;

    va_end(args);

    return result;

#endif
}

bool TRACE_VFormat(const char *fmt, va_list args)
{
#ifdef TRACE_CONFIG_ATOMIC

    bool result = false;

    TRACE_ATOMIC_RESTORESTATE
    {
        result = TRACE_vprintf(fmt, args) >= 0;
    }

    return result;

#else

    return TRACE_vprintf(fmt, args) >= 0;

#endif
}

#endif
