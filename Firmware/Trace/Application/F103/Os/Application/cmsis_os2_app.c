#include "cmsis_os2.h"
#include "os_app.h"
#include "trace.h"
#include "cmsis_os2_app.h"


__NO_RETURN void CMSIS_OS2_APP_Start(void)
{
    osKernelInitialize();


    OS_APP_InitialiseOs(osPriorityNormal5);    

    TRACE_InitialiseOs(osPriorityNormal7);


    osKernelStart();


    /* Should never get here. */
    while (true)
    {
    }
}
