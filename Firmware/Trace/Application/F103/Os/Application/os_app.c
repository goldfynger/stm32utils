#include <stdbool.h>
#include <stdio.h>
#include "stm32f1xx_hal.h"
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "main.h"
#include "trace.h"
#include "os_app.h"


#define OS_APP_TRACE_DATA_TIMER_PERIOD  4000


static osThreadId_t _osTaskHandle;
static osThreadAttr_t _osTask_attributes = {
  .name = "osTask",
  .priority = (osPriority_t) osPriorityLow7,
  .stack_size = 128 * 4
};

static osTimerId_t _traceDataTimerHandle;
static const osTimerAttr_t _traceDataTimer_attributes = {
  .name = "traceDataTimer"
};

static CMSIS_OS2_EX_TaskStateTypeDef _osTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;

static char _128LengthString[] = "This is 128-length string    32|                             64|                             96|                          126|\r\n";
static char _256LengthString[] = "This is 256-length string    32|                             64|                             96|                            128|" \
    "                            160|                            192|                            224|                          254|\r\n";


__NO_RETURN void OS_APP_Task(void *argument);
void OS_APP_TraceDataTimerCallback(void *argument);


osStatus_t OS_APP_InitialiseOs(osPriority_t maxTaskPriority)
{
    osStatus_t osStatus = osOK;

    if ((osStatus = OS_APP_DeinitialiseOs()) != osOK)
    {
        return osStatus;
    }


    _traceDataTimerHandle = osTimerNew(OS_APP_TraceDataTimerCallback, osTimerPeriodic, NULL, &_traceDataTimer_attributes);

    if (_traceDataTimerHandle == NULL)
    {
        Error_Handler();
        return OS_APP_DeinitialiseOs();
    }

    if ((osStatus = osTimerStart(_traceDataTimerHandle, OS_APP_TRACE_DATA_TIMER_PERIOD)) != osOK)
    {
        Error_Handler();
        return osStatus;
    }


    _osTaskHandle = osThreadNew(OS_APP_Task, NULL, &_osTask_attributes);

    if (_osTaskHandle == NULL)
    {
        Error_Handler();
        return OS_APP_DeinitialiseOs();
    }


    return osOK;
}

osStatus_t OS_APP_DeinitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_osTaskHandle != NULL)
    {
        if ((osStatus = osThreadTerminate(_osTaskHandle)) != osOK)
        {
            Error_Handler();
            return osStatus;
        }

        _osTaskHandle = NULL;
    }


    if (_traceDataTimerHandle != NULL)
    {
        if ((osStatus = osTimerDelete(_traceDataTimerHandle)) != osOK)
        {
            Error_Handler();
            return osStatus;
        }

        _traceDataTimerHandle = NULL;
    }


    _osTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
    __force_stores();

    return osOK;
}


static __NO_RETURN void OS_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    _osTaskState = CMSIS_OS2_EX_TASK_STATE_START;
    __force_stores();

    TRACE_Format("%s initialised.\r\n", __FUNCTION__);

    _osTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;
    __force_stores();


    TRACE_String("Start trace.\r\n");
    TRACE_Format("Trace from file %s function %s at line %u.\r\n", __FILE__, __FUNCTION__, __LINE__);
    TRACE_String(_128LengthString);
    TRACE_String(_256LengthString);
    
    while (true)
    {
        TRACE_Format("Current TICK value from task is %u.\r\n", HAL_GetTick());
        
        osDelay(4000);
    }
}

static void OS_APP_TraceDataTimerCallback(void *argument)
{
    TRACE_Format("Current TICK value from OS timer is %u.\r\n", HAL_GetTick());
}
