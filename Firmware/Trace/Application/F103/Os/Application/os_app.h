#ifndef __OS_APP_H
#define __OS_APP_H


#include "cmsis_os2.h"


osStatus_t OS_APP_InitialiseOs(osPriority_t maxTaskPriority);
osStatus_t OS_APP_DeinitialiseOs(void);


#endif /* __OS_APP_H */
