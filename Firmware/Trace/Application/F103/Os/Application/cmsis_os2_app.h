#ifndef __CMSIS_OS2_APP_H
#define __CMSIS_OS2_APP_H


#include "cmsis_os2.h"


__NO_RETURN void CMSIS_OS2_APP_Start(void);


#endif /* __CMSIS_OS2_APP_H */
