#ifndef __TRACE_CONFIG_H
#define __TRACE_CONFIG_H


/* Allow trace via SWO. */
//#define TRACE_CONFIG_SWO

/* Allow trace via UART. */
#define TRACE_CONFIG_SERIAL

/* UART handle that used for trace. No mutex used here, so selected UART must not be used by another application parts. 
 * Ignored when TRACE_CONFIG_SERIAL is not defined. */
#define TRACE_CONFIG_UART_HANDLE            huart1

/* Add incremented number before each string. */
#define TRACE_CONFIG_ADD_STRING_NUMBER

/* Use atomic printf if OS2 not used. Interrupts are disabled while output string is created.
 * Ignored when TRACE_CONFIG_OS2 is defined. */
#define TRACE_CONFIG_ATOMIC

/* Trace using OS mechanisms. */
//#define TRACE_CONFIG_OS2

/* Default size of memory allocation to write format message.
 * Ignored when TRACE_CONFIG_OS2 is not defined. */
#define TRACE_CONFIG_FORMAT_DEFAULT_SIZE    64U

/* Maximum size of memory allocation to write format message. If not equal to TRACE_CONFIG_FORMAT_DEFAULT_SIZE then reallocation allowed.
 * Ignored when TRACE_CONFIG_OS2 is not defined. */
#define TRACE_CONFIG_FORMAT_MAX_SIZE        256U

/* Size of OS queue to transmit trace messages.
 * Ignored when TRACE_CONFIG_OS2 is not defined. */
#define TRACE_CONFIG_QUEUE_COUNT            16U


#endif /* __TRACE_CONFIG_H */
