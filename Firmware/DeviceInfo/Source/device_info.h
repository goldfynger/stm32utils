#ifndef __DEVICE_INFO_H
#define __DEVICE_INFO_H


#include <stdint.h>


typedef enum
{
    DEVICE_INFO_MODEL_PROTOTYPE                             = ((uint8_t)0x00),

    DEVICE_INFO_MODEL_WIRELESS_THERMOMETER_SENSOR_F030F4    = ((uint8_t)0x01),
    DEVICE_INFO_MODEL_WIRELESS_THERMOMETER_SENSOR_F103C8    = ((uint8_t)0x02),

    /* Reserved for future use. */

    DEVICE_INFO_MODEL_NRF24_USB_MODEM_F103C8                = ((uint8_t)0x09),

    /* Reserved for future use. */

    DEVICE_INFO_MODEL_NOT_SET_OR_UNKNOWN                    = ((uint8_t)0xFD),
    DEVICE_INFO_MODEL_EXAMPLE                               = ((uint8_t)0xFE),
    DEVICE_INFO_MODEL_INVALID                               = ((uint8_t)0xFF),
}
DEVICE_INFO_ModelTypeDef;

typedef enum
{
    DEVICE_INFO_REVISION_PROTOTYPE                          = ((uint8_t)0x00),

    /* Numeric revision values. */

    DEVICE_INFO_REVISION_NOT_SET_OR_UNKNOWN                 = ((uint8_t)0xFD),
    DEVICE_INFO_REVISION_EXAMPLE                            = ((uint8_t)0xFE),
    DEVICE_INFO_REVISION_INVALID                            = ((uint8_t)0xFF),
}
DEVICE_INFO_RevisionTypeDef;

typedef enum
{
    /* Numeric version values. */

    DEVICE_INFO_VERSION_NOT_SET_OR_UNKNOWN                  = ((uint8_t)0xFD),
    DEVICE_INFO_VERSION_EXAMPLE                             = ((uint8_t)0xFE),
    DEVICE_INFO_VERSION_INVALID                             = ((uint8_t)0xFF),
}
DEVICE_INFO_VersionTypeDef;

typedef enum
{
    /* Numeric subversion values. */

    DEVICE_INFO_SUBVERSION_NOT_SET_OR_UNKNOWN               = ((uint8_t)0xFD),
    DEVICE_INFO_SUBVERSION_EXAMPLE                          = ((uint8_t)0xFE),
    DEVICE_INFO_SUBVERSION_INVALID                          = ((uint8_t)0xFF),
}
DEVICE_INFO_SubversionTypeDef;


typedef struct
{
    DEVICE_INFO_ModelTypeDef        Model;

    DEVICE_INFO_RevisionTypeDef     Revision;

    DEVICE_INFO_VersionTypeDef      Version;
    DEVICE_INFO_SubversionTypeDef   Subversion;
}
DEVICE_INFO_InformationTypeDef;

typedef struct
{
    uint32_t DeviceUid_0;
    uint32_t DeviceUid_1;
    uint32_t DeviceUid_2;
}
DEVICE_INFO_UidTypeDef;


const DEVICE_INFO_InformationTypeDef * DEVICE_INFO_GetInformation(void);
const DEVICE_INFO_UidTypeDef * DEVICE_INFO_GetUid(void);


#endif /* __DEVICE_INFO_H */
