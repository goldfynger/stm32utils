#include "device_info.h"
#include "device_info_config.h"

#if defined STM32F030x6
    #include "stm32f0xx_hal.h"
#elif defined STM32F103xB
    #include "stm32f1xx_hal.h"
#else
    #error "Device not specified."
#endif


#ifndef DEVICE_INFO_MODEL
#define DEVICE_INFO_MODEL       DEVICE_INFO_MODEL_UNKNOWN
#endif

#ifndef DEVICE_INFO_REVISION
#define DEVICE_INFO_REVISION    DEVICE_INFO_REVISION_UNKNOWN
#endif

#ifndef DEVICE_INFO_VERSION
#define DEVICE_INFO_VERSION     DEVICE_INFO_VERSION_UNKNOWN
#endif

#ifndef DEVICE_INFO_SUBVERSION
#define DEVICE_INFO_SUBVERSION  DEVICE_INFO_SUBVERSION_UNKNOWN
#endif


const DEVICE_INFO_InformationTypeDef _deviceInformation =
{
    .Model = DEVICE_INFO_MODEL,
    .Revision = DEVICE_INFO_REVISION,
    .Version = DEVICE_INFO_VERSION,
    .Subversion = DEVICE_INFO_SUBVERSION,
};


const DEVICE_INFO_InformationTypeDef * DEVICE_INFO_GetInformation(void)
{
    return &_deviceInformation;
}

const DEVICE_INFO_UidTypeDef * DEVICE_INFO_GetUid(void)
{
    return (const DEVICE_INFO_UidTypeDef *)UID_BASE;
}
