#include <stddef.h>
#include <stdint.h>
#include "crc32.h"

#if defined STM32F103xB
    #include "stm32f1xx_hal.h"
#else
    #error "Device not specified."
#endif


/* For better performance input buffer should be 4-bit aligned. */
/* Poly: 0x4C11DB7, Init: 0xFFFFFFFF, RefIn: true, RefOut: true, XorOut: 0xFFFFFFFF, Check: 0xCBF43926 */
/* Based on: */
/* http://we.easyelectronics.ru/STM32/crc32-na-stm32-kak-na-kompe-ili-na-kompe-kak-na-stm32.html */
/* stm32f1xx_hal_crc.c HAL_CRC_Calculate function. */
/* Test here: http://www.sunshine2k.de/coding/javascript/crc/crc_js.html (main CRC32).*/
uint32_t CRC32_Calculate(CRC_HandleTypeDef *hcrc, __I uint8_t pBuffer[], uint32_t bufferLength)
{
    uint32_t temp = 0U; /* CRC output (read from hcrc->Instance->DR register) */

    uint32_t bufferAddress = (uint32_t)pBuffer;
    uint32_t bufferValue = 0;
    size_t len32 = bufferLength >> 2;

    /* Change CRC peripheral state */
    hcrc->State = HAL_CRC_STATE_BUSY;

    /* Reset CRC Calculation Unit (hcrc->Instance->INIT is written in hcrc->Instance->DR) */
    __HAL_CRC_DR_RESET(hcrc);

    /* Enter 32-bit input data to the CRC calculator */
    uint8_t alignment = ((bufferAddress % 4) == 0) ? 4 : (bufferAddress % 2) == 0 ? 2 : 1;

    switch (alignment)
    {
        case 4:
        {
            while (len32--)
            {
                bufferValue = *(uint32_t *)bufferAddress;

                hcrc->Instance->DR = __RBIT(bufferValue);

                bufferAddress += 4;
            }

            bufferValue = *(uint32_t *)bufferAddress;
        }
        break;

        case 2:
        {
            while (len32--)
            {
                bufferValue = *(uint16_t *)(bufferAddress + 2);
                bufferValue <<= 16;
                bufferValue += *(uint16_t *)(bufferAddress);

                hcrc->Instance->DR = __RBIT(bufferValue);

                bufferAddress += 4;
            }

            bufferValue = *(uint16_t *)(bufferAddress + 2);
            bufferValue <<= 16;
            bufferValue += *(uint16_t *)(bufferAddress);
        }
        break;

        default:
        {
            while (len32--)
            {
                bufferValue = *(uint8_t *)(bufferAddress + 3);
                bufferValue <<= 8;
                bufferValue += *(uint8_t *)(bufferAddress + 2);
                bufferValue <<= 8;
                bufferValue += *(uint8_t *)(bufferAddress + 1);
                bufferValue <<= 8;
                bufferValue += *(uint8_t *)(bufferAddress);

                hcrc->Instance->DR = __RBIT(bufferValue);

                bufferAddress += 4;
            }

            bufferValue = *(uint8_t *)(bufferAddress + 3);
            bufferValue <<= 8;
            bufferValue += *(uint8_t *)(bufferAddress + 2);
            bufferValue <<= 8;
            bufferValue += *(uint8_t *)(bufferAddress + 1);
            bufferValue <<= 8;
            bufferValue += *(uint8_t *)(bufferAddress);
        }
        break;
    }

    temp = __RBIT(hcrc->Instance->DR);
    size_t len8 = bufferLength % 4;

    /* Enter 8-bit remainder (if exists).*/
    if (len8)
    {
        hcrc->Instance->DR = __RBIT(temp);

        switch (len8)
        {
            case 1:
            {
                hcrc->Instance->DR = __RBIT((bufferValue & 0x000000FF) ^ temp) >> 24;
                temp = (temp >> 8) ^ __RBIT(hcrc->Instance->DR);
            }
            break;

            case 2:
            {
                hcrc->Instance->DR = (__RBIT((bufferValue & 0x0000FFFF) ^ temp) >> 16);
                temp = (temp >> 16) ^ __RBIT(hcrc->Instance->DR);
            }
            break;

            case 3:
            {
                hcrc->Instance->DR = __RBIT((bufferValue & 0x00FFFFFF) ^ temp) >> 8;
                temp = (temp >> 24) ^ __RBIT(hcrc->Instance->DR);
            }
            break;
        }
    }

    /* Change CRC peripheral state */
    hcrc->State = HAL_CRC_STATE_READY;

    /* Return the CRC computed value */
    return ~temp;
}
