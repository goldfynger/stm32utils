#ifndef __CRC32_H
#define __CRC32_H


#include <stdint.h>

#if defined STM32F103xB
    #include "stm32f1xx_hal.h"
#else
    #error "Device not specified."
#endif


uint32_t CRC32_Calculate(CRC_HandleTypeDef *hcrc, __I uint8_t pBuffer[], uint32_t bufferLength);


#endif /* __CRC32_H */
