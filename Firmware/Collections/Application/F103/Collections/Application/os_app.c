#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include "stm32f1xx_hal.h"
#include "cmsis_os2.h"
#include "cmsis_os2_ex.h"
#include "linked_list.h"
#include "main.h"
#include "trace.h"
#include "os_app.h"


static osThreadId_t _osTaskHandle;
static osThreadAttr_t _osTask_attributes = {
  .name = "osTask",
  .priority = (osPriority_t) osPriorityLow7,
  .stack_size = 128 * 4
};

static CMSIS_OS2_EX_TaskStateTypeDef _osTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;


static __NO_RETURN void OS_APP_Task(void *argument);
static bool OS_APP_LinkedListTest(void);


osStatus_t OS_APP_InitialiseOs(osPriority_t maxTaskPriority)
{
    osStatus_t osStatus = osOK;

    if ((osStatus = OS_APP_DeinitialiseOs()) != osOK)
    {
        return osStatus;
    }


    _osTaskHandle = osThreadNew(OS_APP_Task, NULL, &_osTask_attributes);

    if (_osTaskHandle == NULL)
    {
        Error_Handler();
        return OS_APP_DeinitialiseOs();
    }


    return osOK;
}

osStatus_t OS_APP_DeinitialiseOs(void)
{
    osStatus_t osStatus = osOK;


    if (_osTaskHandle != NULL)
    {
        if ((osStatus = osThreadTerminate(_osTaskHandle)) != osOK)
        {
            Error_Handler();
            return osStatus;
        }

        _osTaskHandle = NULL;
    }


    _osTaskState = CMSIS_OS2_EX_TASK_STATE_IDLE;
    __force_stores();

    return osOK;
}



static __NO_RETURN void OS_APP_Task(void *argument)
{
    TRACE_Format("%s started.\r\n", __FUNCTION__);

    _osTaskState = CMSIS_OS2_EX_TASK_STATE_START;
    __force_stores();


    TRACE_Format("%s initialised.\r\n", __FUNCTION__);

    _osTaskState = CMSIS_OS2_EX_TASK_STATE_RUN;
    __force_stores();


    TRACE_String("\r\n");
    TRACE_String("Start LinkedList\r\n");
    TRACE_String("\r\n");


    OS_APP_LinkedListTest();


    while (true)
    {
        osDelay(osWaitForever);
    }
}

static bool OS_APP_LinkedListTest()
{    
    const uint32_t idxCount = 10;
    const size_t bufferLength = 10;
    LINKED_LIST_NodeTypeDef *pHead = NULL;
    
    
    size_t heapFreeSize = osExGetFreeHeapSize();


    if (LINKED_LIST_RemoveHead(&pHead) != NULL) /* Head not exist yet and should be NULL. */
    {
        TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
        return false;
    }


    if (LINKED_LIST_RemoveNode(&pHead, 0) != NULL) /* List is empty now and node can not be removed. */
    {
        TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
        return false;
    }


    if (LINKED_LIST_GetNode(&pHead, 1) != NULL) /* List is empty now and node can not be get. */
    {
        TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
        return false;
    }


    if (LINKED_LIST_GetCount(&pHead) != 0) /* Count of empty list must be 0. */
    {
        TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
        return false;
    }


    for (int32_t idx = 0; idx < idxCount; idx++) /* Add nodes to list. */
    {
        char *pBuffer = osExMalloc(bufferLength);

        if (pBuffer == NULL)
        {
            TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
            return false;
        }


        if (snprintf(pBuffer, bufferLength, "Value %i", idx) != 7)
        {
            TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
            return false;
        }


        LINKED_LIST_NodeTypeDef *pNewHead = LINKED_LIST_AddHead(&pHead, idx, pBuffer);

        if (pNewHead == NULL || pNewHead != pHead)
        {
            TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
            return false;
        }
    }


    if (LINKED_LIST_GetCount(&pHead) != idxCount) /* Check count of list. */
    {
        TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
        return false;
    }


    for (int32_t idx = 0; idx < idxCount; idx++) /* Print all nodes. */
    {
        LINKED_LIST_NodeTypeDef *pNode = LINKED_LIST_GetNode(&pHead, idx);

        if (pNode == NULL)
        {
            TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
            return false;
        }


        TRACE_Format("Key:%i \"%s\".\r\n", idx, pNode->Value);
    }

    TRACE_String("\r\n");


    for (int32_t idx = 0; idx < idxCount; idx++) /* Remove all odd nodes. */
    {
        if (idx % 2 != 0)
        {
            LINKED_LIST_NodeTypeDef *pNode = LINKED_LIST_RemoveNode(&pHead, idx);
            
            if (pNode == NULL)
            {
                TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
                return false;
            }
            
            osExFree(pNode->Value);
            osExFree(pNode);
        }
    }


    if (LINKED_LIST_GetCount(&pHead) != idxCount / 2) /* Check count of list. */
    {
        TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
        return false;
    }


    for (int32_t idx = 0; idx < idxCount; idx += 2) /* Print all even nodes. */
    {
        LINKED_LIST_NodeTypeDef *pNode = LINKED_LIST_GetNode(&pHead, idx);

        if (pNode == NULL)
        {
            TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
            return false;
        }


        TRACE_Format("Key:%i \"%s\".\r\n", idx, pNode->Value);
    }

    TRACE_String("\r\n");


    LINKED_LIST_NodeTypeDef *pPreviousHead = pHead;

    if (LINKED_LIST_RemoveHead(&pHead) != pPreviousHead) /* Remove head (node with key 8 here). */
    {
        TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
        return false;
    }

    osExFree(pPreviousHead->Value);
    osExFree(pPreviousHead);


    if (LINKED_LIST_GetCount(&pHead) != (idxCount / 2) - 1) /* Check count of list. */
    {
        TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
        return false;
    }


    for (int32_t idx = 0; idx < idxCount - 2; idx += 2) /* Print all nodes exept removed head. */
    {
        LINKED_LIST_NodeTypeDef *pNode = LINKED_LIST_GetNode(&pHead, idx);

        if (pNode == NULL)
        {
            TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
            return false;
        }


        TRACE_Format("Key:%i \"%s\".\r\n", idx, pNode->Value);
    }

    TRACE_String("\r\n");


    while (pHead != NULL) /* Remove all nodes. */
    {
        LINKED_LIST_NodeTypeDef *pRemovedHead = LINKED_LIST_RemoveHead(&pHead);
        
        osExFree(pRemovedHead->Value);
        osExFree(pRemovedHead);
    }


    if (osExGetFreeHeapSize() != heapFreeSize)
    {
        TRACE_Format("%s (%u) error\r\n", __FUNCTION__, __LINE__);
        return false;
    }
    

    return true;
}
