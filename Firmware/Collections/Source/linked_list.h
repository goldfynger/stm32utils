#ifndef __LINKED_LIST_H
#define __LINKED_LIST_H


#include <stdint.h>


typedef struct __LINKED_LIST_NodeTypeDef
{
    int32_t                             Key;     /* The node key used to find the node. Key uniqueness is not guaranteed. */
    void                                * Value; /* Pointer to current node value. */

    struct __LINKED_LIST_NodeTypeDef    * Next;  /* Pointer to next node. Current node is last node in list if next is NULL. */
}
LINKED_LIST_NodeTypeDef;


LINKED_LIST_NodeTypeDef * LINKED_LIST_AddHead   (LINKED_LIST_NodeTypeDef **ppHead, int32_t key, void *pValue);
LINKED_LIST_NodeTypeDef * LINKED_LIST_RemoveHead(LINKED_LIST_NodeTypeDef **ppHead);
LINKED_LIST_NodeTypeDef * LINKED_LIST_RemoveNode(LINKED_LIST_NodeTypeDef **ppHead, int32_t key);
LINKED_LIST_NodeTypeDef * LINKED_LIST_GetNode   (LINKED_LIST_NodeTypeDef **ppHead, int32_t key);
uint32_t                  LINKED_LIST_GetCount  (LINKED_LIST_NodeTypeDef **ppHead);


#endif /* __LINKED_LIST_H */
