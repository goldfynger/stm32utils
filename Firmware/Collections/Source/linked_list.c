#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include "cmsis_os2_ex.h"
#include "linked_list.h"


/* Based on https://www.tutorialspoint.com/data_structures_algorithms/linked_list_program_in_c.htm */


/* Allocates new node and adds it to the list as new head. Key uniqueness is not guaranteed.
 * Returns pointer to new node or NULL if node allocation is faulted. */
LINKED_LIST_NodeTypeDef * LINKED_LIST_AddHead(LINKED_LIST_NodeTypeDef **ppHead, int32_t key, void *pValue)
{
    LINKED_LIST_NodeTypeDef *pNode = osExMalloc(sizeof(LINKED_LIST_NodeTypeDef));

    if (pNode == NULL)
    {
        /* Node allocation fault. */
        return NULL;
    }

    /* No memset because all fields set further. */

    pNode->Key = key;
    pNode->Value = pValue;

    /* Created node is new head; previous head node is next for new head. */
    pNode->Next = *ppHead;
    *ppHead = pNode;

    return pNode;
}

/* Removes head node of list if list not empty. Returned node must be freed by calling code.
 * Returns pointer to removed node or NULL if list was empty. */
LINKED_LIST_NodeTypeDef * LINKED_LIST_RemoveHead(LINKED_LIST_NodeTypeDef **ppHead)
{
    if (*ppHead == NULL)
    {
        /* List is empty. */
        return NULL;
    }

    LINKED_LIST_NodeTypeDef *pCurrent = *ppHead;

    *ppHead = (*ppHead)->Next;

    return pCurrent;
}

/* Looks for a node in list using node key and removes node from list if it was found. Returned node must be freed by calling code.
 * Returns pointer to removed node if node was found or NULL if node not found in list. */
LINKED_LIST_NodeTypeDef * LINKED_LIST_RemoveNode(LINKED_LIST_NodeTypeDef **ppHead, int32_t key)
{
    if (*ppHead == NULL)
    {
        /* List is empty. */
        return NULL;
    }

    LINKED_LIST_NodeTypeDef *pCurrent = *ppHead;
    LINKED_LIST_NodeTypeDef *pPrevious = NULL;

    while (pCurrent->Key != key)
    {
        /* Check for last node. */
        if(pCurrent->Next == NULL)
        {
             return NULL;
        }
        else
        {
            pPrevious = pCurrent;
            pCurrent = pCurrent->Next;
        }
    }

    if (pCurrent == *ppHead)
    {
        /* Remove first node. */
        *ppHead = (*ppHead)->Next;
    }
    else
    {
        /* Remove one of next nodes. */
        pPrevious->Next = pCurrent->Next;
    }

    return pCurrent;
}

/* Looks for a node in list using node key.
 * Returns pointer to founded node or NULL if node not found in list. */
LINKED_LIST_NodeTypeDef * LINKED_LIST_GetNode(LINKED_LIST_NodeTypeDef **ppHead, int32_t key)
{
    if (*ppHead == NULL)
    {
        /* List is empty. */
        return NULL;
    }

    LINKED_LIST_NodeTypeDef *pCurrent = *ppHead;

    while (pCurrent->Key != key)
    {
        /* Check for last node. */
        if(pCurrent->Next == NULL)
        {
             return NULL;
        }
        else
        {
            pCurrent = pCurrent->Next;
        }
    }

    return pCurrent;
}

/* Returns count of nodes in list. */
uint32_t LINKED_LIST_GetCount(LINKED_LIST_NodeTypeDef **ppHead)
{
    uint32_t length = 0;

    for (LINKED_LIST_NodeTypeDef *pCurrent = *ppHead; pCurrent != NULL; pCurrent = pCurrent->Next)
    {
        length += 1;
    }

    return length;
}
