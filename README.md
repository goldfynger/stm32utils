STM32 firmware libraries.
  

|Library                                      |Description                    |
|---------------------------------------------|-------------------------------|
|[**CmsisOs2Ex**](Firmware/CmsisOs2Ex)        |CMSIS OS2 extensions.          |
|                                             |                               |
|[**Crc32**](Firmware/Crc32/Source)           |STM32F1 CRC-32 extensions.     |
|                                             |                               |
|[**DeviceInfo**](Firmware/DeviceInfo)        |Device classification library. |
|                                             |                               |
|[**LinkedList**](Firmware/LinkedList)        |Linked list implementation.    |
|                                             |                               |
|[**StreamToPacket**](Firmware/StreamToPacket)|Stream to packet library.      |
|                                             |                               |
|[**Trace**](Firmware/Trace/Source)           |Trace library.                 |



