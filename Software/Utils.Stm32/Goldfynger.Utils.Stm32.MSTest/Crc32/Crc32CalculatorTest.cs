﻿using Goldfynger.Utils.Stm32.Crc32;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.Utils.Stm32.MSTest.Crc32
{
    [TestClass]
    public class Crc32CalculatorTest
    {
        [TestMethod]
        public void CalculateHardwareChecksumTest()
        {
            const uint fullArrayResult = 0x9EEDE06;
            const uint firstUintResult = 0x468C380E;

            Assert.AreEqual(fullArrayResult, Crc32Calculator.CalculateHardwareChecksum(__32BitArray));
            Assert.AreEqual(firstUintResult, Crc32Calculator.CalculateHardwareChecksum(new uint[] { __32BitArray[0] }));
            Assert.AreEqual(fullArrayResult, Crc32Calculator.CalculateHardwareChecksum(new uint[] { __32BitArray[1] }, firstUintResult));
        }

        [TestMethod]
        public void ComputeSoftwareChecksumTest()
        {
            const uint checkArrayResult = 0xCBF43926;
            const uint fullArrayResult = 0xDB9B78E6;
            const uint oneByteResult = 0x1D41B76;
            const uint twoByteResult = 0x6DD35A80;
            const uint threeByteResult = 0x9DB0CE85;
            const uint fourByteResult = 0x384A187A;

            Assert.AreEqual(checkArrayResult, Crc32Calculator.CalculateSoftwareChecksum(__checkArray));
            Assert.AreEqual(fullArrayResult, Crc32Calculator.CalculateSoftwareChecksum(__8BitArray));


            Assert.AreEqual(oneByteResult, Crc32Calculator.CalculateSoftwareChecksum(new byte[] { __8BitArray[0] }));
            Assert.AreEqual(twoByteResult, Crc32Calculator.CalculateSoftwareChecksum(new byte[] { __8BitArray[0], __8BitArray[1] }));
            Assert.AreEqual(threeByteResult, Crc32Calculator.CalculateSoftwareChecksum(new byte[] { __8BitArray[0], __8BitArray[1], __8BitArray[2] }));
            Assert.AreEqual(fourByteResult, Crc32Calculator.CalculateSoftwareChecksum(new byte[] { __8BitArray[0], __8BitArray[1], __8BitArray[2], __8BitArray[3] }));
        }


        private static readonly byte[] __checkArray = { 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39 };
        private static readonly byte[] __8BitArray = { 0x67, 0x45, 0x23, 0x01, 0xEF, 0xCD, 0xAB, 0x89 };
        private static readonly uint[] __32BitArray = { 0x01234567, 0x89ABCDEF };
    }
}
