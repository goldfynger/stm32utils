﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Goldfynger.Utils.Stm32.StreamToPacket;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.Utils.Stm32.MSTest.StreamToPacket
{
    [TestClass]
    public class StreamToPacketMakerTest
    {
        [TestMethod]
        public void MakeStreamMakePacketTest()
        {
            var array = new byte[4] { 1, 2, 3, 4 };

            var streamBuffer = StreamToPacketMaker.MakeStream(array);

            var stream = new TestStream(streamBuffer);

            var packetTask = StreamToPacketMaker.MakePacketAsync(stream);
            packetTask.Wait();
            var packet = packetTask.Result;

            Assert.AreEqual(array.Length, packet.Count);
            Assert.AreEqual(array[0], packet[0]);
            Assert.AreEqual(array[1], packet[1]);
            Assert.AreEqual(array[2], packet[2]);
            Assert.AreEqual(array[3], packet[3]);
        }


        private class TestStream : Stream
        {
            private readonly byte[] _data;

            private long _position;


            public TestStream(IList<byte> data)
            {
                _data = data.ToArray();
                _position = 0;
            }


            public override bool CanRead => true;

            public override bool CanSeek => throw new NotSupportedException();

            public override bool CanWrite => throw new NotSupportedException();

            public override long Length => _data.Length;

            public override long Position
            {
                get => _position;
                set => _position = value;
            }


            public override void Flush() => throw new NotSupportedException();

            public override int Read(byte[] buffer, int offset, int count)
            {
                var toCopy = Math.Min(_data.Length - _position, count);

                Array.Copy(_data, _position, buffer, offset, toCopy);
                _position += toCopy;

                return (int)toCopy;
            }

            public override long Seek(long offset, SeekOrigin origin) => throw new NotSupportedException();

            public override void SetLength(long value) => throw new NotSupportedException();

            public override void Write(byte[] buffer, int offset, int count) => throw new NotSupportedException();
        }
    }
}
