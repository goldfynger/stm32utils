﻿using System;
using System.Linq;

using Goldfynger.Utils.Stm32.DeviceInfo;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Goldfynger.Utils.Stm32.MSTest.DeviceInfo
{
    [TestClass]
    public class DeviceInfoTest
    {
        [TestMethod]
        public void InformationTest()
        {
            var model = new Model(Model.DeviceModels.DEVICE_INFO_MODEL_WIRELESS_THERMOMETER_SENSOR_F030F4);
            var revision = new Revision(Revision.DeviceRevisions.DEVICE_INFO_REVISION_EXAMPLE);
            var version = new Stm32.DeviceInfo.Version(Stm32.DeviceInfo.Version.DeviceVersions.DEVICE_INFO_VERSION_EXAMPLE);
            var subversion = new Subversion(Subversion.DeviceSubversions.DEVICE_INFO_SUBVERSION_EXAMPLE);

            var impl1 = new Information(model, revision, version, subversion);
            var impl2 = new Information(new byte[] { model.Value, revision.Value, version.Value, subversion.Value });

            Assert.AreEqual(impl1, impl2);
            Assert.AreEqual(true, impl1 == impl2);
        }

        [TestMethod]
        public void UidTest()
        {
            uint uid0 = 0x03020100;
            uint uid1 = 0x07060504;
            uint uid2 = 0x0B0A0908;

            var impl1 = new Uid(uid0, uid1, uid2);
            var impl2 = new Uid(BitConverter.GetBytes(uid0).Concat(BitConverter.GetBytes(uid1)).Concat(BitConverter.GetBytes(uid2)).ToArray());

            Assert.AreEqual(impl1, impl2);
            Assert.AreEqual(true, impl1 == impl2);
        }
    }
}
