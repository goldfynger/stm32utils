﻿using System;

namespace Goldfynger.Utils.Stm32.DeviceInfo
{
    /// <summary>
    /// Device subversion.
    /// </summary>
    public sealed record Subversion : IEquatable<Subversion>
    {
        /// <summary>
        /// Creates new device subversion using <see cref="byte"/> value as a source.
        /// </summary>
        /// <param name="value"><see cref="byte"/> value.</param>
        public Subversion(byte value)
        {
            Value = value;
            DeviceSubversion = (DeviceSubversions)value;
        }

        /// <summary>
        /// Creates new device version using one of <see cref="DeviceSubversions"/> enumeration values as a source.
        /// </summary>
        /// <param name="subversion"><see cref="Subversion"/> value.</param>
        public Subversion(DeviceSubversions subversion)
        {
            Value = (byte)subversion;
            DeviceSubversion = subversion;
        }


        /// <summary>
        /// Device subversion as <see cref="byte"/> value.
        /// </summary>
        public byte Value { get; }

        /// <summary>
        /// One of <see cref="DeviceSubversions"/> enumeration values or <see cref="byte"/> value if enumeration has no specified subversion.
        /// </summary>
        public DeviceSubversions DeviceSubversion { get; }


        /// <summary>
        /// Device subversion.
        /// </summary>
        /// <returns><see cref="string"/> with device subversion.</returns>
        public override string ToString()
        {
            return Enum.IsDefined(typeof(DeviceSubversions), Value) ? ((DeviceSubversions)Value).ToString() : Value.ToString();
        }


        /// <summary>
        /// Specific software subversion values.
        /// </summary>
        public enum DeviceSubversions : byte
        {
            /// <summary>Device software subversion value that used when subversion is not set or unknown.</summary>
            DEVICE_INFO_SUBVERSION_NOT_SET_OR_UNKNOWN = 0xFD,
            /// <summary>Device software subversion value that used when firmware is example.</summary>
            DEVICE_INFO_SUBVERSION_EXAMPLE = 0xFE,
            /// <summary>Device software subversion value that used as invalid subversion.</summary>
            DEVICE_INFO_SUBVERSION_INVALID = 0xFF,
        }
    }
}
