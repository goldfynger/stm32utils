﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.Utils.Stm32.DeviceInfo
{
    /// <summary>
    /// Device uinque identifier.
    /// </summary>
    public sealed record Uid : IEquatable<Uid>
    {
        /// <summary>
        /// Size of raw representation in bytes.
        /// </summary>
        public const int Size = 12;


        /// <summary>
        /// Creates new device information usnig <see cref="uint"/> parts of unique identifier as a source.
        /// </summary>
        /// <param name="uid0">Zero-indexed first part of unique identifier.</param>
        /// <param name="uid1">Zero-indexed second part of unique identifier.</param>
        /// <param name="uid2">Zero-indexed third part of unique identifier.</param>
        public Uid(uint uid0, uint uid1, uint uid2)
        {
            Uid0 = uid0;
            Uid1 = uid1;
            Uid2 = uid2;

            Raw = new ReadOnlyCollection<byte>(new UnmanagedUid { Uid0 = uid0, Uid1 = uid1, Uid2 = uid2 }.ToByteArray());
        }

        /// <summary>
        /// Creates new device UID usnig <see cref="IList{T}"/> of <see cref="byte"/> as a source.
        /// </summary>
        /// <param name="raw">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        public Uid(IList<byte> raw)
        {
            if (raw == null)
            {
                throw new ArgumentNullException(nameof(raw));
            }

            if (raw.Count != Size)
            {
                throw new ArgumentException($"Size of source must be equal to {Size}.", nameof(raw));
            }

            var unmanagedUid = raw.ToArray().ToStruct<UnmanagedUid>();

            Uid0 = unmanagedUid.Uid0;
            Uid1 = unmanagedUid.Uid1;
            Uid2 = unmanagedUid.Uid2;

            Raw = new ReadOnlyCollection<byte>(raw.ToArray());
        }


        /// <summary>
        /// First 32 bits of UID.
        /// </summary>
        public uint Uid0 { get; }

        /// <summary>
        /// Second 32 bits of UID.
        /// </summary>
        public uint Uid1 { get; }

        /// <summary>
        /// Third 32 bits of UID.
        /// </summary>
        public uint Uid2 { get; }

        /// <summary>
        /// Raw unique identifier as <see cref="ReadOnlyCollection{T}"/> of <see cref="byte"/>.
        /// </summary>
        public ReadOnlyCollection<byte> Raw { get; }


        /// <summary>
        /// Device UID.
        /// </summary>
        /// <returns><see cref="string"/> with UID.</returns>
        public override string ToString() => $"{Uid0:X8}:{Uid1:X8}:{Uid2:X8}";

        /// <summary>
        /// Returns a value indicating whether this instance and a specified <see cref="Uid "/> object represent the same value.
        /// </summary>
        /// <param name="other">An object to compare to this instance.</param>
        /// <returns><see langword="true"/> if <paramref name="other"/> is equal to this instance; otherwise, <see langword="false"/>.</returns>
        public bool Equals(Uid other)
        {
            if (other is null)
            {
                return false;
            }

            if (ReferenceEquals(this, other))
            {
                return true;
            }

            if (GetType() != other.GetType())
            {
                return false;
            }

            return (Uid0 == other.Uid0) && (Uid1 == other.Uid1) && (Uid2 == other.Uid2);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A hash code for the current <see cref="Uid"/>.</returns>
        public override int GetHashCode() => HashCode.Combine(Uid0, Uid1, Uid2);


        [StructLayout(LayoutKind.Explicit, Size = Size)]
        private struct UnmanagedUid
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U4)] internal uint Uid0;
            [FieldOffset(4)] [MarshalAs(UnmanagedType.U4)] internal uint Uid1;
            [FieldOffset(8)] [MarshalAs(UnmanagedType.U4)] internal uint Uid2;
        }
    }
}
