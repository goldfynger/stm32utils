﻿using System;

namespace Goldfynger.Utils.Stm32.DeviceInfo
{
    /// <summary>
    /// Device version.
    /// </summary>
    public sealed record Version : IEquatable<Version>
    {
        /// <summary>
        /// Creates new device version using <see cref="byte"/> value as a source.
        /// </summary>
        /// <param name="value"><see cref="byte"/> value.</param>
        public Version(byte value)
        {
            Value = value;
            DeviceVersion = (DeviceVersions)value;
        }

        /// <summary>
        /// Creates new device version using one of <see cref="DeviceVersions"/> enumeration values as a source.
        /// </summary>
        /// <param name="version"><see cref="Version"/> value.</param>
        public Version(DeviceVersions version)
        {
            Value = (byte)version;
            DeviceVersion = version;
        }


        /// <summary>
        /// Device version as <see cref="byte"/> value.
        /// </summary>
        public byte Value { get; }

        /// <summary>
        /// One of <see cref="DeviceVersions"/> enumeration values or <see cref="byte"/> value if enumeration has no specified version.
        /// </summary>
        public DeviceVersions DeviceVersion { get; }


        /// <summary>
        /// Device version.
        /// </summary>
        /// <returns><see cref="string"/> with device version.</returns>
        public override string ToString()
        {
            return Enum.IsDefined(typeof(DeviceVersions), Value) ? ((DeviceVersions)Value).ToString() : Value.ToString();
        }


        /// <summary>
        /// Specific software version values.
        /// </summary>
        public enum DeviceVersions : byte
        {
            /// <summary>Device software version value that used when version is not set or unknown.</summary>
            DEVICE_INFO_VERSION_NOT_SET_OR_UNKNOWN = 0xFD,
            /// <summary>Device software version value that used when firmware is example.</summary>
            DEVICE_INFO_VERSION_EXAMPLE = 0xFE,
            /// <summary>Device software version value that used as invalid version.</summary>
            DEVICE_INFO_VERSION_INVALID = 0xFF,
        }
    }
}
