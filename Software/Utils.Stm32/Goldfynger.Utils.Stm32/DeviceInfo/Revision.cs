﻿using System;

namespace Goldfynger.Utils.Stm32.DeviceInfo
{
    /// <summary>
    /// Device revision.
    /// </summary>
    public sealed record Revision : IEquatable<Revision>
    {
        /// <summary>
        /// Creates new device revision using <see cref="byte"/> value as a source.
        /// </summary>
        /// <param name="value"><see cref="byte"/> value.</param>
        public Revision(byte value)
        {
            Value = value;
            DeviceRevision = (DeviceRevisions)value;
        }

        /// <summary>
        /// Creates new device revision using one of <see cref="DeviceRevisions"/> enumeration values as a source.
        /// </summary>
        /// <param name="revision"><see cref="DeviceRevisions"/> value.</param>
        public Revision(DeviceRevisions revision)
        {
            Value = (byte)revision;
            DeviceRevision = revision;
        }


        /// <summary>
        /// Device revision as <see cref="byte"/> value.
        /// </summary>
        public byte Value { get; }

        /// <summary>
        /// One of <see cref="DeviceRevisions"/> enumeration values or <see cref="byte"/> value if enumeration has no specified revision.
        /// </summary>
        public DeviceRevisions DeviceRevision { get; }


        /// <summary>
        /// Device revision.
        /// </summary>
        /// <returns><see cref="string"/> with device revision.</returns>
        public override string ToString()
        {
            return Enum.IsDefined(typeof(DeviceRevisions), Value) ? ((DeviceRevisions)Value).ToString() : Value.ToString();
        }


        /// <summary>
        /// Specific hardware revision values.
        /// </summary>
        public enum DeviceRevisions : byte
        {
            /// <summary>Device revision value that used for prototypes.</summary>
            DEVICE_INFO_REVISION_PROTOTYPE = 0x00,

            /// <summary>Device revision value that used when revision is not set or unknown.</summary>
            DEVICE_INFO_REVISION_NOT_SET_OR_UNKNOWN = 0xFD,
            /// <summary>Device revision value that used when firmware is example.</summary>
            DEVICE_INFO_REVISION_EXAMPLE = 0xFE,
            /// <summary>Device revision value that used as invalid revision.</summary>
            DEVICE_INFO_REVISION_INVALID = 0xFF,
        }
    }
}
