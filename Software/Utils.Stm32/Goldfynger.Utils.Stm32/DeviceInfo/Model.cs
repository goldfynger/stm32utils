﻿using System;

namespace Goldfynger.Utils.Stm32.DeviceInfo
{
    /// <summary>
    /// Device model.
    /// </summary>
    public sealed record Model : IEquatable<Model>
    {
        /// <summary>
        /// Creates new device model using <see cref="byte"/> value as a source.
        /// </summary>
        /// <param name="value"><see cref="byte"/> value.</param>
        public Model(byte value)
        {
            Value = value;
            DeviceModel = (DeviceModels)value;
        }

        /// <summary>
        /// Creates new device model using one of <see cref="DeviceModels"/> enumeration values as a source.
        /// </summary>
        /// <param name="model"><see cref="DeviceModels"/> value.</param>
        public Model(DeviceModels model)
        {
            Value = (byte)model;
            DeviceModel = model;
        }


        /// <summary>
        /// Device model as <see cref="byte"/> value.
        /// </summary>
        public byte Value { get; }

        /// <summary>
        /// One of <see cref="DeviceModels"/> enumeration values or <see cref="byte"/> value if enumeration has no specified model.
        /// </summary>
        public DeviceModels DeviceModel { get; }


        /// <summary>
        /// Device model.
        /// </summary>
        /// <returns><see cref="string"/> with device model.</returns>
        public override string ToString()
        {
            return Enum.IsDefined(typeof(DeviceModels), Value) ? ((DeviceModels)Value).ToString() : Value.ToString();
        }


        /// <summary>
        /// Known device hardware models and specific model values.
        /// </summary>
        public enum DeviceModels : byte
        {
            /// <summary>Device model value that used for prototypes.</summary>
            DEVICE_INFO_MODEL_PROTOTYPE = 0x00,

            /// <summary>Wireless thermometer sensor with STM32F030F4 MCU.</summary>
            DEVICE_INFO_MODEL_WIRELESS_THERMOMETER_SENSOR_F030F4 = 0x01,
            /// <summary>Wireless thermometer sensor with STM32F103C8 MCU.</summary>
            DEVICE_INFO_MODEL_WIRELESS_THERMOMETER_SENSOR_F103C8 = 0x02,

            /// <summary>NRF24 USB modem with STM32F103C8 MCU.</summary>
            DEVICE_INFO_MODEL_NRF24_USB_MODEM_F103C8 = 0x09,

            /// <summary>Device model value that used when model is not set or unknown.</summary>
            DEVICE_INFO_MODEL_NOT_SET_OR_UNKNOWN = 0xFD,
            /// <summary>Device model value that used when firmware is example.</summary>
            DEVICE_INFO_MODEL_EXAMPLE = 0xFE,
            /// <summary>Device model value that used as invalid model.</summary>
            DEVICE_INFO_MODEL_INVALID = 0xFF,
        }
    }
}
