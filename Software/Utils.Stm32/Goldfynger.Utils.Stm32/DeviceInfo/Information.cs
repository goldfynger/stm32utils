﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;

using Goldfynger.Utils.Extensions;

namespace Goldfynger.Utils.Stm32.DeviceInfo
{
    /// <summary>
    /// Device information.
    /// </summary>
    public sealed record Information : IEquatable<Information>
    {
        /// <summary>
        /// Size of raw representation in bytes.
        /// </summary>
        public const int Size = 4;


        /// <summary>
        /// Creates new device information usnig <see cref="DeviceInfo.Model"/>, <see cref="DeviceInfo.Revision"/>, <see cref="DeviceInfo.Version"/>, <see cref="DeviceInfo.Subversion"/> as a source.
        /// </summary>
        /// <param name="model">Device model.</param>
        /// <param name="revision">Device revision.</param>
        /// <param name="version">Device version.</param>
        /// <param name="subversion">Device subversion.</param>
        public Information(Model model, Revision revision, Version version, Subversion subversion)
        {
            Model = model ?? throw new ArgumentNullException(nameof(model));
            Revision = revision ?? throw new ArgumentNullException(nameof(revision));
            Version = version ?? throw new ArgumentNullException(nameof(version));
            Subversion = subversion ?? throw new ArgumentNullException(nameof(subversion));

            Raw = new ReadOnlyCollection<byte>(new UnmanagedInformation { Model = model.Value, Revision = revision.Value, Version = version.Value, Subversion = subversion.Value }.ToByteArray());
        }

        /// <summary>
        /// Creates new device information usnig <see cref="IList{T}"/> of <see cref="byte"/> as a source.
        /// </summary>
        /// <param name="raw">Source <see cref="IList{T}"/> of <see cref="byte"/>.</param>
        public Information(IList<byte> raw)
        {
            if (raw == null)
            {
                throw new ArgumentNullException(nameof(raw));
            }

            if (raw.Count != Size)
            {
                throw new ArgumentException($"Size of source must be equal to {Size}.", nameof(raw));
            }

            var unmanagedInformation = raw.ToArray().ToStruct<UnmanagedInformation>();

            Model = new Model(unmanagedInformation.Model);
            Revision = new Revision(unmanagedInformation.Revision);
            Version = new Version(unmanagedInformation.Version);
            Subversion = new Subversion(unmanagedInformation.Subversion);

            Raw = new ReadOnlyCollection<byte>(raw.ToArray());
        }


        /// <summary>
        /// Device model.
        /// </summary>
        public Model Model { get; }

        /// <summary>
        /// Device revision.
        /// </summary>
        public Revision Revision { get; }

        /// <summary>
        /// Device version.
        /// </summary>
        public Version Version { get; }

        /// <summary>
        /// Device subversion.
        /// </summary>
        public Subversion Subversion { get; }

        /// <summary>
        /// Raw information as <see cref="ReadOnlyCollection{T}"/> of <see cref="byte"/>.
        /// </summary>
        public ReadOnlyCollection<byte> Raw { get; }


        /// <summary>
        /// Device info.
        /// </summary>
        /// <returns><see cref="string"/> with device info.</returns>
        public override string ToString() => $"{Model}.{Revision}.{Version}.{Subversion}";

        /// <summary>
        /// Returns a value indicating whether this instance and a specified <see cref="Information"/> object represent the same value.
        /// </summary>
        /// <param name="other">An object to compare to this instance.</param>
        /// <returns><see langword="true"/> if <paramref name="other"/> is equal to this instance; otherwise, <see langword="false"/>.</returns>
        public bool Equals(Information other)
        {
            if (other is null)
            {
                return false;
            }
          
            if (ReferenceEquals(this, other))
            {
                return true;
            }

            if (GetType() != other.GetType())
            {
                return false;
            }

            return (Model == other.Model) && (Revision == other.Revision) && (Version == other.Version) && (Subversion == other.Subversion);
        }

        /// <summary>
        /// Returns the hash code for this instance.
        /// </summary>
        /// <returns>A hash code for the current <see cref="Information"/>.</returns>
        public override int GetHashCode() => HashCode.Combine(Model, Revision, Version, Subversion);


        [StructLayout(LayoutKind.Explicit, Size = Size)]
        private struct UnmanagedInformation
        {
            [FieldOffset(0)] [MarshalAs(UnmanagedType.U1)] internal byte Model;
            [FieldOffset(1)] [MarshalAs(UnmanagedType.U1)] internal byte Revision;
            [FieldOffset(2)] [MarshalAs(UnmanagedType.U1)] internal byte Version;
            [FieldOffset(3)] [MarshalAs(UnmanagedType.U1)] internal byte Subversion;
        }
    }
}
