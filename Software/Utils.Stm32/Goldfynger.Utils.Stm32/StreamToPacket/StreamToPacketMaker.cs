﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Goldfynger.Utils.Extensions;
using Goldfynger.Utils.Stm32.Crc32;

namespace Goldfynger.Utils.Stm32.StreamToPacket
{
    /// <summary>
    /// Contains static methods to convert byte packet to stream representation and back.
    /// </summary>
    /// <remarks>Can be used to send array of bytes over unreliable chanels like serial port. Result stream has checksums for data integrity.</remarks>
    public static class StreamToPacketMaker
    {
        /*
        Protocol low level:
        -------------------------------------------------------------------------------------------
        | DLE (1) | STX (1) | Protocol high level data. DLE bytes duplicates  | DLE (1) | ETX (1) |
        -------------------------------------------------------------------------------------------

        Protocol high level:
        -------------------------------------------------------------------------------------------
        |             Application level data              |  CRC32 of application level data (4)  |
        -------------------------------------------------------------------------------------------
        */


        private const byte STX = 0x02;
        private const byte ETX = 0x03;
        private const byte DLE = 0x10;


        /// <summary>
        /// Asynchronously сonverts stream to original data array using readable <see cref="Stream"/> and <see cref="CancellationToken"/>.
        /// </summary>
        /// <param name="stream"><see cref="Stream"/> with <see cref="Stream.CanRead"/> is <see langword="true"/> with stream representation.</param>
        /// <param name="cancellationToken"><see cref="CancellationToken"/> for operation cancellation.</param>
        /// <returns>Original data.</returns>
        public static async Task<IList<byte>> MakePacketAsync(Stream stream, CancellationToken cancellationToken = default)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (!stream.CanRead)
            {
                throw new ArgumentException("Stream must be readable.", nameof(stream));
            }

            cancellationToken.ThrowIfCancellationRequested();

            var readBuffer = new byte[1];
            var state = State.WaitForFirstDLE;
            var packetBuffer = new List<byte>();

            while (true)
            {
                cancellationToken.ThrowIfCancellationRequested();

                var length = await stream.ReadAsync(readBuffer, cancellationToken);

                cancellationToken.ThrowIfCancellationRequested();

                if (length == 1)
                {
                    var value = readBuffer[0];

                    switch (state)
                    {
                        case State.WaitForFirstDLE:
                            {
                                if (value == DLE)
                                {
                                    state = State.WaitForSTX;
                                }
                            }
                            break;

                        case State.WaitForSTX:
                            {
                                if (value == STX)
                                {
                                    state = State.WaitForData;
                                }
                                else
                                {
                                    state = State.WaitForFirstDLE;
                                }
                            }
                            break;

                        case State.WaitForData:
                            {
                                if (value == DLE)
                                {
                                    state = State.WaitForETXOrSecondDLE;
                                }
                                else
                                {
                                    packetBuffer.Add(value);
                                }
                            }
                            break;

                        case State.WaitForETXOrSecondDLE:
                            {
                                if (value == ETX)
                                {
                                    if (packetBuffer.Count > 4)
                                    {
                                        var packetBufferArray = packetBuffer.ToArray();
                                        var packetSize = packetBuffer.Count - 4;
                                        var packet = packetBufferArray.Copy(0, packetSize);

                                        var originalCrc32 = BitConverter.ToUInt32(packetBufferArray, packetSize);
                                        var calculatedCrc32 = Crc32Calculator.CalculateSoftwareChecksum(packet);

                                        if (originalCrc32 == calculatedCrc32)
                                        {
                                            return packet;
                                        }
                                    }

                                    packetBuffer.Clear();
                                    state = State.WaitForFirstDLE;
                                }
                                else if (value == DLE)
                                {
                                    packetBuffer.Add(value);
                                    state = State.WaitForData;
                                }
                                else
                                {
                                    packetBuffer.Clear();
                                    state = State.WaitForFirstDLE;
                                }
                            }
                            break;
                    }
                }
                else
                {
                    throw new InvalidOperationException($"Read completed with result {length}; expected 1.");
                }
            }            
        }

        /// <summary>
        /// Converts <see cref="IList{T}"/> of <see cref="byte"/> to stream representation.
        /// </summary>
        /// <param name="packet">Original data to convert.</param>
        /// <returns>Stream representation.</returns>
        public static IList<byte> MakeStream(IList<byte> packet)
        {
            if (packet == null)
            {
                throw new ArgumentNullException(nameof(packet));
            }

            if (packet.Count == 0)
            {
                throw new ArgumentException("Packet size must be greater than zero.", nameof(packet));
            }

            var packetWithCrc32 = new List<byte>(packet);
            packetWithCrc32.AddRange(BitConverter.GetBytes(Crc32Calculator.CalculateSoftwareChecksum(packet)));

            var streamBuffer = new List<byte>
            {
                DLE,
                STX
            };

            foreach (var value in packetWithCrc32)
            {
                streamBuffer.Add(value);

                if (value == DLE)
                {
                    streamBuffer.Add(DLE);
                }
            }

            streamBuffer.Add(DLE);
            streamBuffer.Add(ETX);

            return streamBuffer;
        }


        private enum State
        {
            WaitForFirstDLE,
            WaitForSTX,
            WaitForData,
            WaitForETXOrSecondDLE,
        }
    }
}
